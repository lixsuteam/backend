package com.felixsu.springcommon.client.entity;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Movie;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by felixsoewito on 11/16/16.
 */

public class MovieEntityClient extends BaseEntityClient<Movie, String> {

    public MovieEntityClient(RestTemplate restTemplate, String baseUrl, Class clazz) {
        super(restTemplate, baseUrl, clazz);
    }

    public List<Movie> findAll(String status, String genre, String city, String owner) throws FSNotFoundException {
        Map<String, String> map = new HashMap<>();
        map.put("status", status);
        map.put("genre", genre);
        map.put("city", city);
        map.put("owner", owner);

        String uri = buildUrlWithParam(baseUrl, map);

        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<Movie> request = new HttpEntity<>(null, headers);

        RestResponse<Movie> result;

        try {
            result = doExchange(uri, HttpMethod.GET, request);
        } catch (FsBadRequestException e) {
            throw new RuntimeException(e);
        }

        return result.getResponses();
    }

    public void batchUpdate(List<Movie> movies, String owner) throws FSNotFoundException, FsBadRequestException {
        Map<String, String> map = new HashMap<>();
        map.put("owner", owner);

        String url = buildUrlWithParam(baseUrl + "/movies", map);

        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<List<Movie>> request = new HttpEntity<>(movies, headers);

        doExchange(url, HttpMethod.PUT, request);
    }
}
