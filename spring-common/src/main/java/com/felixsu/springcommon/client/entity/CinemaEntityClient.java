package com.felixsu.springcommon.client.entity;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Cinema;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by felixsoewito on 11/16/16.
 */

public class CinemaEntityClient extends BaseEntityClient<Cinema, String> {

    public CinemaEntityClient(RestTemplate restTemplate, String baseUrl, Class clazz) {
        super(restTemplate, baseUrl, clazz);
    }

    public List<Cinema> findAll(String city, String owner) throws FSNotFoundException {
        Map<String, String> map = new HashMap<>();
        map.put("city", city);
        map.put("owner", owner);

        String url = buildUrlWithParam(baseUrl, map);

        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<Cinema> request = new HttpEntity<>(null, headers);

        RestResponse<Cinema> result;

        try {
            result = doExchange(url, HttpMethod.GET, request);
        } catch (FsBadRequestException e) {
            throw new RuntimeException(e);
        }

        return result.getResponses();
    }

    public void saveOrUpdate(Cinema cinema) throws FSNotFoundException, FsBadRequestException {

        Cinema persistedCinema;

        try {
            persistedCinema = findOne(cinema.getId());
        } catch (FSNotFoundException e) {
            persistedCinema = null;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        if (persistedCinema != null) {
            persistedCinema.setCover(cinema.getCover());
            persistedCinema.setCity(cinema.getCity());
            persistedCinema.setCinemaName(cinema.getCinemaName());

            update(persistedCinema);
        } else {
            save(cinema);
        }
    }

    public void batchUpdate(List<Cinema> cinemas, String owner) throws FSNotFoundException, FsBadRequestException {
        Map<String, String> map = new HashMap<>();
        map.put("owner", owner);

        String url = buildUrlWithParam(baseUrl + "/cinemas", map);

        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<List<Cinema>> request = new HttpEntity<>(cinemas, headers);

        doExchange(url, HttpMethod.PUT, request);
    }
}
