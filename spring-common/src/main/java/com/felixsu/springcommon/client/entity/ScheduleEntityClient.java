package com.felixsu.springcommon.client.entity;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.common.model.cinema.Schedule;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by felixsoewito on 11/16/16.
 */

public class ScheduleEntityClient extends BaseEntityClient<Schedule, Long> {

    public ScheduleEntityClient(RestTemplate restTemplate, String baseUrl, Class clazz) {
        super(restTemplate, baseUrl, clazz);
    }

    public List<Schedule> findAll(String city, String movieId, String cinemaId) throws FSNotFoundException {
        Map<String, String> map = new HashMap<>();
        map.put("city", city);
        map.put("movie", movieId);
        map.put("cinema", cinemaId);

        String url = buildUrlWithParam(baseUrl, map);

        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<Schedule> request = new HttpEntity<>(headers);

        RestResponse<Schedule> result;

        try {
            result = doExchange(url, HttpMethod.GET, request);
        } catch (FsBadRequestException e) {
            throw new RuntimeException(e);
        }

        return result.getResponses();
    }

    public List<Schedule> findAllSchedule(Cinema cinema) throws FSNotFoundException {
        String url = baseUrl.concat("/cinema/" + cinema.getId());
        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<Schedule> request = new HttpEntity<>(headers);

        RestResponse<Schedule> result;

        try {
            result = doExchange(url, HttpMethod.GET, request);
        } catch (FsBadRequestException e) {
            throw new RuntimeException(e);
        }

        return result.getResponses();
    }

    public List<Schedule> findAllSchedule(Movie movie) throws FSNotFoundException {
        String url = baseUrl.concat("/movie/" + movie.getId());
        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<Schedule> request = new HttpEntity<>(headers);

        RestResponse<Schedule> result;

        try {
            result = doExchange(url, HttpMethod.GET, request);
        } catch (FsBadRequestException e) {
            throw new RuntimeException(e);
        }

        return result.getResponses();

    }

    public void updateSchedules(String cinemaId, List<Schedule> schedules) {
        String url = baseUrl.concat("/schedules/" + cinemaId);
        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<List<Schedule>> request = new HttpEntity<>(schedules, headers);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
    }
}
