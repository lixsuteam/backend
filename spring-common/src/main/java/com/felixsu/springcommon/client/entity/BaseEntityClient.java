package com.felixsu.springcommon.client.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.felixsu.common.model.general.Error;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.helper.HttpHelper;
import com.felixsu.common.helper.JsonHelper;
import com.felixsu.common.model.EntityModel;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.general.ErrorWrapper;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by felixsoewito on 11/16/16.
 */
public class BaseEntityClient<M extends EntityModel, ID extends Serializable> {

    protected final String baseUrl;
    protected final RestTemplate restTemplate;
    protected final Class clazz;
    private ObjectMapper mapper;

    public BaseEntityClient(RestTemplate restTemplate, String baseUrl, Class clazz) {
        this.restTemplate = restTemplate;
        this.clazz = clazz;
        this.baseUrl = baseUrl.endsWith("/") ? baseUrl.substring(0, baseUrl.length()-1) : baseUrl;
        this.mapper = new ObjectMapper();
    }

    public M save(M model) {
        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<M> request = new HttpEntity<>(model, headers);

        RestResponse<M> result;
        try {
            result = doExchange(baseUrl, HttpMethod.POST, request);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result.getResponse();
    }

    public M findOne(ID id) throws FSNotFoundException {
        String url = baseUrl + "/" + id;
        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<M> request = new HttpEntity<>(headers);

        RestResponse<M> result;

        try {
            result = doExchange(url, HttpMethod.GET, request);
        } catch (FSNotFoundException e) {
            throw new FSNotFoundException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result.getResponse();
    }

    public M update(M model) throws FSNotFoundException, FsBadRequestException {
        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<M> request = new HttpEntity<>(model, headers);

        RestResponse<M> result;

        try {
            result = doExchange(baseUrl, HttpMethod.PUT, request);
        } catch (FSNotFoundException e) {
            throw new FSNotFoundException(e);
        } catch (FsBadRequestException e){
            throw new FsBadRequestException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result.getResponse();
    }

    public void delete(ID id) throws FSNotFoundException {
        String url = baseUrl + "/" + id;

        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<M> request = new HttpEntity<>(null, headers);

        try {
            doExchange(url, HttpMethod.DELETE, request);
        } catch (FSNotFoundException e) {
            throw new FSNotFoundException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    protected HttpHeaders buildDefaultHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHelper.SERVICE_ID, "9002");
        headers.set(HttpHelper.SERVICE_NAME, "CORE");

        List<MediaType> acceptMedias = new ArrayList<>();
        acceptMedias.add(MediaType.APPLICATION_JSON);
        acceptMedias.add(MediaType.APPLICATION_XML);

        headers.setAccept(acceptMedias);
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    protected RestResponse<M> extractResult(ResponseEntity<String> response) {
        RestResponse<M> result;
        try {
            if (response.hasBody()) {
                String bodyJson = response.getBody();
                result = mapper.readValue(bodyJson, mapper.getTypeFactory().constructParametricType(RestResponse.class, clazz));
            } else {
                throw new IOException("body have nothing to extract");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    protected String buildUrlWithParam(String url, Map<String, String> param) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);

        for (Map.Entry<String, String> entry : param.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            if (value != null) {
                builder.queryParam(key, value);
            }
        }

        return builder.build().encode().toUri().toString();
    }

    protected void validateResponse(HttpStatus status, Error error)
            throws FSNotFoundException,FsBadRequestException {

        if (status != HttpStatus.OK) {
            String errorMessage = error != null ? error.getMessage() : "Unknown Error";
            switch (status) {
                case NOT_FOUND :
                    throw new FSNotFoundException(errorMessage);
                case BAD_REQUEST :
                    throw new FsBadRequestException(errorMessage);
                case INTERNAL_SERVER_ERROR :
                    throw new RuntimeException(errorMessage);
                default:
                    throw new RuntimeException(errorMessage);
            }
        }
    }

    protected RestResponse<M> doExchange(String url, HttpMethod method, HttpEntity request) throws FSNotFoundException,
            FsBadRequestException, RuntimeException {

        RestResponse<M> result = null;

        try {
            ResponseEntity<String> response = restTemplate.exchange(url, method, request, String.class);
            result = extractResult(response);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            String errorString = e.getResponseBodyAsString();

            HttpStatus errorStatus = e.getStatusCode();
            Error error;
            try {
                ErrorWrapper errorWrapper = JsonHelper.fromJson(errorString, ErrorWrapper.class);
                error = errorWrapper.getError();
            } catch (IOException ex) {
                error = Error.fromThrowable("500", ex);
                errorStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            }
            validateResponse(errorStatus, error);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return result;

    }
}
