package com.felixsu.springcommon.client.entity;

import com.felixsu.common.model.cinema.CinemaDetail;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by felixsoewito on 12/8/16.
 * Cinema Detail Client for doing transaction with entity service
 */
public class CinemaDetailClient extends BaseEntityClient<CinemaDetail, Long> {

    public CinemaDetailClient(RestTemplate restTemplate, String baseUrl, Class clazz) {
        super(restTemplate, baseUrl, clazz);
    }

    public void batchUpdate(String cinemaId, List<CinemaDetail> details) {
        String url = baseUrl + "/" + cinemaId;

        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<List<CinemaDetail>> request = new HttpEntity<>(details, headers);

        try {
            doExchange(url, HttpMethod.PUT, request);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
