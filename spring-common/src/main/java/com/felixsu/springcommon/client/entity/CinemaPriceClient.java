package com.felixsu.springcommon.client.entity;

import com.felixsu.common.model.cinema.CinemaPrice;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by felixsoewito on 12/8/16.
 * Cinema Price Client for doing transaction with entity service
 */
public class CinemaPriceClient extends BaseEntityClient<CinemaPrice, Long>{

    public CinemaPriceClient(RestTemplate restTemplate, String baseUrl, Class clazz) {
        super(restTemplate, baseUrl, clazz);
    }

    public void batchUpdate(String cinemaId, List<CinemaPrice> prices) {
        String url = baseUrl + "/" + cinemaId;

        HttpHeaders headers = buildDefaultHeaders();

        HttpEntity<List<CinemaPrice>> request = new HttpEntity<>(prices, headers);

        try {
            doExchange(url, HttpMethod.PUT, request);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
