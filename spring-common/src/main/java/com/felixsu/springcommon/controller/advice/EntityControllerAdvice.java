package com.felixsu.springcommon.controller.advice;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.exception.FsMultipleChoicesException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.general.Error;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by felixsoewito on 11/30/16.
 */

@RestControllerAdvice
public class EntityControllerAdvice {

    private static final String STATUS_MULTIPLE_CHOICES = "300";
    private static final String STATUS_BAD_REQUEST = "400";
    private static final String STATUS_NOT_FOUND = "404";
    private static final String STATUS_INTERNAL_SERVER_ERROR = "500";

    @ExceptionHandler(FsBadRequestException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    RestResponse entityBadRequestExceptionHandler(FsBadRequestException e) {
        return new RestResponse<>(null, null, Error.fromThrowable(STATUS_BAD_REQUEST, e));
    }

    @ExceptionHandler(FSNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    RestResponse entityNotFoundExceptionHandler(FSNotFoundException e) {
        return new RestResponse<>(null, null, Error.fromThrowable(STATUS_NOT_FOUND, e));
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    RestResponse entityInternalErrorExceptionHandler(RuntimeException e) {
        return new RestResponse<>(null, null, Error.fromThrowable(STATUS_INTERNAL_SERVER_ERROR, e));
    }

    @ExceptionHandler(FsMultipleChoicesException.class)
    @ResponseStatus(HttpStatus.MULTIPLE_CHOICES)
    RestResponse entityMultipleChoicesExceptionHandler(FsMultipleChoicesException e) {
        return new RestResponse<>(null, null, Error.fromThrowable(STATUS_MULTIPLE_CHOICES, e));
    }
}
