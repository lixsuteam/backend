package com.felixsu.springcommon.controller;

import com.felixsu.common.model.CommonModel;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.general.Error;

import java.util.List;

/**
 * Created by felixsoewito on 11/21/16.
 */
public class BaseController<M extends CommonModel> {

    protected RestResponse<M> wrapList(List<M> m, Error e) {
        return new RestResponse<>(null, m, e);
    }

    protected RestResponse<M> wrapObject(M m, Error e) {
        return new RestResponse<>(m, null, e);
    }
}
