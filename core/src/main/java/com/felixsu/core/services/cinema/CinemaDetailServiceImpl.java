package com.felixsu.core.services.cinema;

import com.felixsu.springcommon.client.entity.CinemaDetailClient;
import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by felixsoewito on 12/8/16.
 */

@Service
public class CinemaDetailServiceImpl implements CinemaDetailService {

    private CinemaDetailClient client;

    @Autowired
    public CinemaDetailServiceImpl(CinemaDetailClient client) {
        this.client = client;
    }

    @Override
    public void detailsUpdate(String cinemaId, List<CinemaDetail> details) {
        client.batchUpdate(cinemaId, details);
    }

    @Override
    public CinemaDetail create(CinemaDetail model) {
        return client.save(model);
    }

    @Override
    public CinemaDetail findOne(Long id) throws FSNotFoundException {
        return client.findOne(id);
    }

    @Override
    public CinemaDetail update(CinemaDetail model) throws FSNotFoundException, FsBadRequestException {
        return client.update(model);
    }

    @Override
    public void delete(Long id) throws FSNotFoundException {
        client.delete(id);
    }
}
