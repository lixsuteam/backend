package com.felixsu.core.services.cinema;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.springcommon.client.entity.CinemaEntityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by felixsoewito on 11/17/16.
 */

@Service
public class CinemaServiceImpl implements CinemaService {

    private CinemaEntityClient client;

    @Autowired
    public CinemaServiceImpl(CinemaEntityClient client) {
        this.client = client;
    }

    @Override
    public Cinema create(Cinema model) {
        return client.save(model);
    }

    @Override
    public Cinema findOne(String id) throws FSNotFoundException {
        return client.findOne(id);
    }

    @Override
    public Cinema update(Cinema model) throws FSNotFoundException, FsBadRequestException {
        return client.update(model);
    }

    @Override
    public void delete(String id) throws FSNotFoundException {
        client.delete(id);
    }

    @Override
    public List<Cinema> findAll(String city, String owner) throws FSNotFoundException {
        return client.findAll(city, owner);
    }
}
