package com.felixsu.core.services.cinema;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.core.services.base.BaseEntityService;

import java.util.List;

/**
 * Created by felixsoewito on 11/16/16.
 */
public interface CinemaService extends BaseEntityService<Cinema, String> {

    List<Cinema> findAll(String city, String owner) throws FSNotFoundException;
}
