package com.felixsu.core.services.base;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.EntityModel;

import java.io.Serializable;

/**
 * Created by felixsoewito on 11/16/16.
 */
public interface BaseEntityService<M extends EntityModel, ID extends Serializable> {

    M create(M model);
    M findOne(ID id) throws FSNotFoundException;
    M update(M model) throws FSNotFoundException, FsBadRequestException;
    void delete(ID id) throws FSNotFoundException;
}
