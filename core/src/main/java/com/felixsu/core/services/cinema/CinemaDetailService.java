package com.felixsu.core.services.cinema;

import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.core.services.base.BaseEntityService;

import java.util.List;

/**
 * Created by felixsoewito on 12/8/16.
 */
public interface CinemaDetailService extends BaseEntityService<CinemaDetail, Long> {

    void detailsUpdate(String cinemaId, List<CinemaDetail> details);

}
