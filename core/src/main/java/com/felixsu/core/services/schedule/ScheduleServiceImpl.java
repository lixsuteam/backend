package com.felixsu.core.services.schedule;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.springcommon.client.entity.ScheduleEntityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by felixsoewito on 11/17/16.
 */

@Service
public class ScheduleServiceImpl implements ScheduleService {

    private ScheduleEntityClient client;

    @Autowired
    public ScheduleServiceImpl(ScheduleEntityClient client) {
        this.client = client;
    }

    @Override
    public Schedule create(Schedule model) {
        return client.save(model);
    }

    @Override
    public Schedule findOne(Long id) throws FSNotFoundException {
        return client.findOne(id);
    }

    @Override
    public Schedule update(Schedule model) throws FSNotFoundException, FsBadRequestException {
        return client.update(model);
    }

    @Override
    public void delete(Long id) throws FSNotFoundException {
        client.delete(id);
    }

    @Override
    public List<Schedule> findAll(String city, String movieId, String cinemaId) throws FSNotFoundException {
        return client.findAll(city, movieId, cinemaId);
    }

    @Override
    public List<Schedule> findAllScheduleForCinema(String cinemaId) throws FSNotFoundException {
        Cinema cinema = new Cinema(cinemaId);
        return client.findAllSchedule(cinema);
    }

    @Override
    public List<Schedule> findAllScheduleForMovie(String movieId) throws FSNotFoundException {
        Movie movie = new Movie(movieId);
        return client.findAllSchedule(movie);
    }
}
