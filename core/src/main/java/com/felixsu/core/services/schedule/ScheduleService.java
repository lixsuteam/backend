package com.felixsu.core.services.schedule;


import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.core.services.base.BaseEntityService;

import java.util.List;

/**
 * Created by felixsoewito on 11/16/16.
 */

public interface ScheduleService  extends BaseEntityService<Schedule, Long> {

    List<Schedule> findAll(String city, String movieId, String cinemaId) throws FSNotFoundException;
    List<Schedule> findAllScheduleForCinema(String cinemaId) throws FSNotFoundException;
    List<Schedule> findAllScheduleForMovie(String movieId) throws FSNotFoundException;

}
