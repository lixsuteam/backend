package com.felixsu.core.services.cinema;

import com.felixsu.springcommon.client.entity.CinemaPriceClient;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.cinema.CinemaPrice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by felixsoewito on 12/8/16.
 */

@Service
public class CinemaPriceServiceImpl implements CinemaPriceService {

    private CinemaPriceClient client;

    @Autowired
    public CinemaPriceServiceImpl(CinemaPriceClient client) {
        this.client = client;
    }

    @Override
    public void detailsUpdate(String cinemaId, List<CinemaPrice> prices) {
        client.batchUpdate(cinemaId, prices);
    }

    @Override
    public CinemaPrice create(CinemaPrice model) {
        return client.save(model);
    }

    @Override
    public CinemaPrice findOne(Long id) throws FSNotFoundException {
        return client.findOne(id);
    }

    @Override
    public CinemaPrice update(CinemaPrice model) throws FSNotFoundException, FsBadRequestException {
        return client.update(model);
    }

    @Override
    public void delete(Long id) throws FSNotFoundException {
        client.delete(id);
    }
}
