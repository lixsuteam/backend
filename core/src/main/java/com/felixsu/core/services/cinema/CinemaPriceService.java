package com.felixsu.core.services.cinema;

import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.core.services.base.BaseEntityService;

import java.util.List;

/**
 * Created by felixsoewito on 12/8/16.
 */
public interface CinemaPriceService extends BaseEntityService<CinemaPrice, Long> {

    void detailsUpdate(String cinemaId, List<CinemaPrice> prices);

}
