package com.felixsu.core.services.movie;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.core.services.base.BaseEntityService;

import java.util.List;

/**
 * Created by felixsoewito on 11/16/16.
 */


public interface MovieService  extends BaseEntityService<Movie, String> {

    List<Movie> findAll(String status, String genre, String city, String owner) throws FSNotFoundException;

}
