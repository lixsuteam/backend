package com.felixsu.core.services.movie;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.springcommon.client.entity.MovieEntityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by felixsoewito on 11/17/16.
 */

@Service
public class MovieServiceImpl implements MovieService {

    private MovieEntityClient client;

    @Autowired
    public MovieServiceImpl(MovieEntityClient client) {
        this.client = client;
    }

    @Override
    public Movie create(Movie model) {
        return client.save(model);
    }

    @Override
    public Movie findOne(String s) throws FSNotFoundException {
        return client.findOne(s);
    }

    @Override
    public Movie update(Movie model) throws FSNotFoundException, FsBadRequestException {
        return client.update(model);
    }

    @Override
    public void delete(String s) throws FSNotFoundException {
        client.delete(s);
    }

    @Override
    public List<Movie> findAll(String status, String genre, String city, String owner) throws FSNotFoundException {
        return client.findAll(status, genre, city, owner);
    }
}
