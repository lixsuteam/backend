package com.felixsu.core;


import com.felixsu.common.model.cinema.*;
import com.felixsu.core.client.CgvClient;
import com.felixsu.springcommon.client.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@SpringBootApplication
@EnableScheduling
@Configuration
@ComponentScan(basePackages = {
        "com.felixsu.core",
        "com.felixsu.springcommon.controller.advice",
        "com.felixsu.springcommon.config"})
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {

        LOGGER.info("Starting CORE Application");

        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            LOGGER.info(beanName);
        }
    }

    @Bean
    CinemaEntityClient createCinemaEntityClient() {
        return new CinemaEntityClient(new RestTemplate(), "http://localhost:9000/cinema", Cinema.class);
    }

    @Bean
    MovieEntityClient createMovieEntityClient() {
        return new MovieEntityClient(new RestTemplate(), "http://localhost:9000/movie", Movie.class);
    }

    @Bean
    ScheduleEntityClient createScheduleEntityClient() {
        return new ScheduleEntityClient(new RestTemplate(), "http://localhost:9000/schedule", Schedule.class);
    }

    @Bean
    CinemaDetailClient createCinemaDetailClient() {
        return new CinemaDetailClient(new RestTemplate(), "http://localhost:9000/cinema/details", CinemaDetail.class);
    }

    @Bean
    CinemaPriceClient createCinemaPriceClient() {
        return new CinemaPriceClient(new RestTemplate(), "http://localhost:9000/cinema/prices", CinemaPrice.class);
    }

    @Bean
    CgvClient createCgvClient() {
        return new CgvClient(createCinemaEntityClient(),
                createScheduleEntityClient(), createMovieEntityClient());
    }
}