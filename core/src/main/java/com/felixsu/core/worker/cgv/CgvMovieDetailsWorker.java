package com.felixsu.core.worker.cgv;

import com.felixsu.common.model.cinema.Movie;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Executable;
import java.util.Arrays;
import java.util.List;

/**
 * Created on 11/29/16.
 *
 * @author felixsoewito
 */
public class CgvMovieDetailsWorker extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(CgvMovieDetailsWorker.class);

    private static final String CLASS_MOVIE_DETAILS = "movie-add-info";
    private static final String CLASS_MOVIE_DESCRIPTION = "movie-synopsis";
    private static final String CLASS_MOVIE_TITLE = "movie-info-title";
    private static final String CLASS_MOVIE_TRAILER = "trailer-section";
    private static final String BASE_URL = "https://www.cgv.id/en/movies";
    private static final String DEFAULT_DETAILS_URL = BASE_URL + "/detail";
    private static final String COMING_SOON_DETAILS_URL =  BASE_URL + "/detail_comingsoon";
    private static final int MAX_TRY = 3;
    private static final int TIMEOUT = 5000;

    private final Movie movie;

    public CgvMovieDetailsWorker(Movie movie) {
        this.movie = movie;
    }

    @Override
    public void run() {

        boolean success = false;
        int counter = 0;

        do {
            counter++;

            String movieId = movie.getId();
            String url = DEFAULT_DETAILS_URL;
            if (movie.getStatus().equals(Movie.Status.STATUS_COMING_SOON)) {
                url = COMING_SOON_DETAILS_URL;
            }

            try {
                LOGGER.debug("trying connect to " + url + "/" + movieId);
                Document doc = Jsoup.connect(url + "/" + movieId).timeout(TIMEOUT).get();

                Element movieDetailsElement = doc.getElementsByClass(CLASS_MOVIE_DETAILS).get(0);
                Element movieDescriptionElement = doc.getElementsByClass(CLASS_MOVIE_DESCRIPTION).get(0);
                Element movieTitleElement = doc.getElementsByClass(CLASS_MOVIE_TITLE).get(0);
                Element movieTrailerElement = doc.getElementsByClass(CLASS_MOVIE_TRAILER).get(0);

                movie.setMovieName(movieTitleElement.text());
                if (movieTrailerElement.children().size() > 0){
                    movie.setTrailerUrl(movieTrailerElement.child(0).attr("src"));
                }

                setMovieDetails(movieDetailsElement, movie);
                setMovieDescription(movieDescriptionElement, movie);

                success = true;
            } catch (Exception e) {
                LOGGER.error("Error on get " + BASE_URL + "/" + movieId + " for movie details of " + movie.getId(), e);
                success = false;
            }

        } while (counter < MAX_TRY && !success);

        if (!success){
            LOGGER.error("failed to get movie details, ID=>" + movie.getId());
        } else {
            LOGGER.info("success to get movie details, ID=>" + movie.getId());
        }
    }

    private void setMovieDescription(Element el, Movie movie) {
        int i = 0;
        String description = "default-description";
        for (Element e : el.children()) {
            String value = e.text();
            if (i == 0) {
                description = value;
            } else if (!value.isEmpty()){
                //try to get english description
                description = value;
            }
            i++;
        }
        movie.setMovieDescription(description);
    }

    private void setMovieDetails(Element el, Movie movie) {
        Elements detailElements = el.getElementsByTag("li");

        int i = 0;
        for (Element de : detailElements) {
            String rawValue = de.text();
            if (rawValue.indexOf(':') == -1) {
                LOGGER.warn(rawValue);
            }
            String value;

            try {
                value = rawValue.substring(rawValue.indexOf(':') + 2);
            } catch (Exception e) {
                LOGGER.info("failed on parsing movie details on element " + rawValue + " with ID " + movie.getId());
                value = "";
            }

            switch (i) {
                case 0 :
                    value = value.isEmpty() ? "NA" : value;
                    movie.setDirectors(value);
                    break;
                case 1 :
                    List<String> actors = Arrays.asList(value.split(", "));
                    StringBuilder sb = new StringBuilder();

                    for (String actor : actors) {
                        sb.append(actor).append('#');
                    }
                    sb.setLength(sb.length() - 1);
                    movie.setActors(sb.toString());
                    break;
                case 2 :
                    String[] rawDuration = value.split(" ");
                    LOGGER.info(rawDuration[0]);
                    int duration;
                    try {
                        duration = Integer.valueOf(rawDuration[0]);
                    } catch (Exception e) {
                        duration = 0;
                    }
                    movie.setDuration(duration);
                    break;
                case 3 :
                    value = value.isEmpty() ? "NA" : value;
                    movie.setRating(value);
                    break;
                case 4 :
                    value = value.isEmpty() ? "NA" : value;
                    movie.setGenre(value);
                    break;
                case 5 :
                    value = value.isEmpty() ? "NA" : value;
                    movie.setLanguage(value);
                    break;
                case 6 :
                    value = value.isEmpty() ? "NA" : value;
                    movie.setSubtitle(value);
                    break;
                default:
                    break;
            }

            i++;
        }
    }
}
