package com.felixsu.core.worker.cgv;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.core.client.CgvClient;
import com.felixsu.springcommon.client.entity.MovieEntityClient;
import com.felixsu.common.helper.DateHelper;
import com.felixsu.core.worker.MovieWorker;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created on 11/24/16.
 *
 * @author felixsoewito
 */
public class CgvMovieWorker extends BaseExecutorWorker implements MovieWorker {

    private static final Logger LOGGER = LoggerFactory.getLogger(CgvMovieWorker.class);

    private static final String CLASS_NOW_PLAYING = "movie-list-body";
    private static final String CLASS_COMING_SOON = "comingsoon-movie-list-body";
    private static final String MOVIE_BASE_URL = "https://www.cgv.id/en/movies/now_playing";
    private static final int MAX_TRY_COUNT = 3;
    private static final int TIMEOUT = 15000;
    private static final int MAX_THREAD = 8;

    private final CgvClient.OnFinishListener listener;
    private final MovieEntityClient client;

    public CgvMovieWorker(CgvClient.OnFinishListener listener, MovieEntityClient client) {
        this.listener = listener;
        this.client = client;
    }

    @Override
    public void run() {
//        int tryCount = 0;
//        String startTime = DateHelper.getIso8601DateFormatterString();
//        boolean success = false;
//        LOGGER.debug("Movie worker start");
//
//        List<Movie> results = new ArrayList<>();
//        Document doc = null;
//
//        do {
//            tryCount++;
//            try {
//                doc = Jsoup.connect(MOVIE_BASE_URL).timeout(TIMEOUT).get();
//                success = true;
//            } catch (Exception e){
//                LOGGER.info("error call movie url on " + startTime + " with counter= "  + tryCount);
//                if (tryCount > MAX_TRY_COUNT){
//                    LOGGER.warn("reach max try call movie url on " + startTime + "with counter= " + tryCount, e);
//                    listener.onFinish(e, null);
//                }
//            }
//        } while (!success && tryCount <= MAX_TRY_COUNT);
//
//        if (!success || doc == null){
//            return;
//        }
//
//        //get all <li> with class city
//        Element nowPlayingSection = doc.getElementsByClass(CLASS_NOW_PLAYING).get(0);
//        Elements tagImgElements = nowPlayingSection.getElementsByTag("img");
//
//        for (Element e : tagImgElements) {
//            Movie m = new Movie();
//            String imageUrl = e.attr("src");
//            String movieId = imageUrl.substring(imageUrl.indexOf("MOV"), imageUrl.indexOf("."));
//
//            m.setCoverUrl(imageUrl);
//            m.setId(movieId);
//            m.setStatus(Movie.Status.STATUS_NOW_PLAYING);
//            m.setOwner(Cinema.CGV);
//            results.add(m);
//        }
//
//        Element comingSoonSection = doc.getElementsByClass(CLASS_COMING_SOON).get(0);
//        Elements tagImgElementsCS = comingSoonSection.getElementsByTag("img");
//
//        for (Element e : tagImgElementsCS) {
//            Movie m = new Movie();
//            String imageUrl = e.attr("src");
//            String movieId = imageUrl.substring(imageUrl.indexOf("MOV"), imageUrl.indexOf("."));
//            m.setCoverUrl(imageUrl);
//            m.setId(movieId);
//            m.setStatus(Movie.Status.STATUS_COMING_SOON);
//            m.setOwner(Cinema.CGV);
//
//            //prevent duplicate for now playing and coming soon
//            if (find(results, m) == null) {
//                results.add(m);
//            }
//        }
//
//        //MOVIES populated by movies at this point :)
//        ExecutorService service = Executors.newFixedThreadPool(MAX_THREAD);
//
//        for (Movie movie : results) {
//            CgvMovieDetailsWorker scheduleWorker = new CgvMovieDetailsWorker(movie);
//            service.execute(scheduleWorker);
//        }
//        service.shutdown();
//
//        shutdownExecutor(service);
//
//        try {
//            client.batchUpdate(results, Cinema.CGV);
//        } catch (Exception e) {
//            LOGGER.error("failed to finalize cinema data!", e);
//        }
//
//        LOGGER.info("movie service terminated: " + service.isTerminated());
//
//        listener.onFinish(null, results);
    }

    private Movie find(List<Movie> movies, Movie movie) {
        for (Movie m : movies) {
            if (m.equals(movie)) {
                return m;
            }
        }

        return null;
    }


}
