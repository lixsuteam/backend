package com.felixsu.core.worker.cgv;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.springcommon.client.entity.CinemaEntityClient;
import com.felixsu.springcommon.client.entity.ScheduleEntityClient;
import com.felixsu.core.worker.ScheduleWorker;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 11/24/16.
 *
 * @author felixsoewito
 */
public class CgvScheduleWorker extends Thread implements ScheduleWorker {

    private static final Logger LOGGER = LoggerFactory.getLogger(CgvScheduleWorker.class);

    private String CLASS_SCHEDULE_TYPE = "schedule-type";
    private String CLASS_SCHEDULE_TITLE = "schedule-title";
    private String CLASS_CINEMA_INFO = "cinema-info";
    private String BASE_URL = "https://www.cgv.id/en/schedule/cinema";
    private int MAX_TRY = 3;
    private int TIMEOUT = 5000;

    private final Element movieElement;
    private final String cityName;
    private Cinema cinema;
    private final List<Schedule> schedules = new ArrayList<>();
    private final List<Cinema> cinemas;
    private final CinemaEntityClient cinemaEntityClient;
    private final ScheduleEntityClient scheduleEntityClient;

    public CgvScheduleWorker(Element movieElement, List<Cinema> cinemas, String cityName,
                             CinemaEntityClient cinemaClient,
                             ScheduleEntityClient scheduleClient) {
        this.movieElement = movieElement;
        this.cinemas = cinemas;
        this.cityName = cityName;
        this.cinemaEntityClient = cinemaClient;
        this.scheduleEntityClient = scheduleClient;
    }

    @Override
    public void run() {
        boolean success;
        int counter = 0;

        do {
            counter++;

            //get <a> as first child of ce
            Element ice = movieElement.select("a").first();
            String cinemaName = ice.attr("title");
            String cinemaId = ice.attr("id");
            cinema = new Cinema(cinemaId, cinemaName, cityName.toLowerCase(), Cinema.CGV);

            try {
                Document movieDoc = Jsoup.connect(BASE_URL + "/" + cinemaId).timeout(TIMEOUT).get();
                Elements movieElements = movieDoc.getElementsByClass(CLASS_SCHEDULE_TITLE);
                Element cinemaInfoElement = movieDoc.getElementsByClass(CLASS_CINEMA_INFO).get(0);
                String coverUrl = cinemaInfoElement.getElementsByTag("img").get(0).attr("src");

                cinema.setCover(coverUrl);

                for (Element me : movieElements){
                    Element ul = me.nextElementSibling();
                    Elements scheduleWrappersElements = ul.getElementsByClass(CLASS_SCHEDULE_TYPE);

                    for (Element swe : scheduleWrappersElements){
                        Schedule schedule = new Schedule(cinemaId, cinemaName);
                        String scheduleClass = swe.text().trim();
                        Elements scheduleTypeElements = swe.nextElementSibling().getElementsByTag("a");

                        int i = 0;
                        final StringBuilder sb = new StringBuilder();

                        for (Element se : scheduleTypeElements){
                            if (i == 0){
                                String price = se.attr("price");
                                String format = se.attr("movieformat");
                                String movieId = se.attr("movie");
                                String type = se.attr("auditype");

                                schedule.setScheduleClass(scheduleClass);
                                schedule.setFormat(format);
                                schedule.setMovieId(movieId);
                                schedule.setType(type);
                                schedule.setPrice(price);
                                schedule.setCity(cityName.toLowerCase());
                            }
                            sb.append(se.text()).append('#');
                            i++;
                        }

                        sb.setLength(sb.length()-1);
                        schedule.setPlayingTime(sb.toString());
                        schedules.add(schedule);
                    }
                }

                success = true;

                synchronized (cinemas) {
                    cinemas.add(cinema);
                }
                cinemaEntityClient.saveOrUpdate(cinema);
                scheduleEntityClient.updateSchedules(cinemaId, schedules);

            } catch (Exception e){
                LOGGER.warn("error get =>" + BASE_URL + "/" + cinemaId +" on try = " + counter, e);
                success = false;
            }
        } while (counter <= MAX_TRY && !success);

        Element ice = movieElement.select("a").first();
        String cinemaName = ice.attr("title");
        String cinemaId = ice.attr("id");

        if (!success){
            LOGGER.error("failed to get cinema schedules details, ID=>" + cinemaId + "-" + cinemaName);
        } else {
            LOGGER.info("success to get cinema schedules details, ID=>" + cinemaId + "-" + cinemaName);
        }
    }
}
