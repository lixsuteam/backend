package com.felixsu.core.worker.cgv;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.core.client.CgvClient;
import com.felixsu.springcommon.client.entity.CinemaEntityClient;
import com.felixsu.springcommon.client.entity.ScheduleEntityClient;
import com.felixsu.common.helper.DateHelper;
import com.felixsu.core.worker.CinemaWorker;
import com.felixsu.core.worker.ScheduleWorker;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created on 11/24/16.
 *
 * @author felixsoewito
 */
public class CgvCinemaWorker extends BaseExecutorWorker implements CinemaWorker {

    private static final Logger LOGGER = LoggerFactory.getLogger(CinemaWorker.class);

    private final String  CITY_ELEMENTS_KEY = "city";
    private final String CGV_BASE_URL = "https://www.cgv.id/en/schedule/cinema";

    private final int MAX_TRY_COUNT = 3;
    private final int TIMEOUT = 15000;
    private final int MAX_THREAD = 8;

    private final CgvClient.OnFinishListener listener;
    private final CinemaEntityClient cinemaEntityClient;
    private final ScheduleEntityClient scheduleEntityClient;

    public CgvCinemaWorker(CgvClient.OnFinishListener listener,
                           CinemaEntityClient cinemaEntityClient,
                           ScheduleEntityClient scheduleEntityClient) {
        this.listener = listener;
        this.cinemaEntityClient = cinemaEntityClient;
        this.scheduleEntityClient = scheduleEntityClient;
    }

    @Override
    public void run() {

//        int tryCount = 0;
//        boolean success = false;
//        String startTime = DateHelper.getIso8601DateFormatterString();
//
//        LOGGER.debug("Cinema worker start");
//
//        final List<Cinema> results = new ArrayList<>();
//        Document doc = null;
//
//        do {
//            try {
//                tryCount++;
//                doc = Jsoup.connect(CGV_BASE_URL).timeout(TIMEOUT).get();
//                success = true;
//            } catch (Exception e){
//                LOGGER.info("error call base cinema url: "+ CGV_BASE_URL + " on " + startTime + "with counter=" +  tryCount);
//                if (tryCount == MAX_TRY_COUNT){
//                    LOGGER.warn("reach max try call cinema url on $startTime with counter= $tryCount", e);
//                    listener.onFinish(e, null);
//                }
//            }
//        } while (!success && tryCount <= MAX_TRY_COUNT);
//
//        if (!success || doc == null){
//            return;
//        }
//
//        //get all <li> with class city
//        Elements cityElements = doc.getElementsByClass(CITY_ELEMENTS_KEY);
//        ExecutorService service = Executors.newFixedThreadPool(MAX_THREAD);
//
//        for (Element e : cityElements){
//            Elements cinemaElements = e.select("ul").first().getElementsByTag("li");
//
//            String cityName = e.select("a").first().text();
//
//            for (Element ce : cinemaElements){
//                ScheduleWorker sw = new CgvScheduleWorker(ce, results, cityName, cinemaEntityClient, scheduleEntityClient);
//                service.execute(sw);
//            }
//        }
//        service.shutdown();
//
//        shutdownExecutor(service);
//
//        try {
//            cinemaEntityClient.batchUpdate(results, Cinema.CGV);
//        } catch (Exception e) {
//            LOGGER.error("failed to finalize cinema data!", e);
//        }
//
//        LOGGER.info("cinema service terminated: " + service);
//        listener.onFinish(null, results);
    }
}
