package com.felixsu.core;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import okhttp3.OkHttpClient;

/**
 * Created on 02/03/18.
 *
 * @author felixsoewito
 */
public class Main {

    public static void main(String[] args) {

        OkHttpClient client = new OkHttpClient();
        Etc etc1 = new Etc();
        Etc etc2 = new Etc();
        System.out.println("Main thread: " + Thread.currentThread().getId());

        Observable<Object> observable = Observable.zip(etc1.getData("x2").onErrorReturn(e -> {
            System.out.println("error 1st call");
            return "xxxxx";
        }), etc2.getData("x2").onErrorReturn(e -> {
            System.out.println("error 2nd call");
            return "yyyyy";
        }), (s1, s2) -> {
            return s1 + s2;
        });

        Disposable disposable = observable.subscribe(s -> System.out.println("result " + s));
        System.out.println("disposed");
//        disposable.dispose();

    }




}
