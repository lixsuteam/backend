package com.felixsu.core.controllers.cinema;

import com.felixsu.springcommon.controller.BaseController;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.core.services.cinema.CinemaPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 12/8/16.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "cinema/prices")
public class CinemaPriceControllerImpl extends BaseController<CinemaPrice> implements CinemaPriceController {

    private CinemaPriceService service;

    @Autowired
    public CinemaPriceControllerImpl(CinemaPriceService service) {
        this.service = service;
    }

    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<CinemaPrice> detailsUpdate(
            @PathVariable(name = "id") String cinemaId,
            @RequestBody List<CinemaPrice> prices) {
        service.detailsUpdate(cinemaId, prices);
        return wrapObject(null, null);
    }
}
