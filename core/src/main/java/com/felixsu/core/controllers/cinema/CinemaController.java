package com.felixsu.core.controllers.cinema;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Cinema;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public interface CinemaController {

    RestResponse<Cinema> create(Cinema model);

    RestResponse<Cinema> findAll(String city, String owner) throws FSNotFoundException;

    RestResponse<Cinema> findOne(String id) throws FSNotFoundException;

}
