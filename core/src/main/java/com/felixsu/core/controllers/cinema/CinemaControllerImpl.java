package com.felixsu.core.controllers.cinema;

import com.felixsu.springcommon.controller.BaseController;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.RestResponse;
import com.felixsu.core.services.cinema.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 11/7/16.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "cinema")
public class CinemaControllerImpl extends BaseController<Cinema> implements CinemaController {

    private final CinemaService service;

    @Autowired
    public CinemaControllerImpl(CinemaService service) {
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Cinema> create(@RequestBody() Cinema model) {
        Cinema result = service.create(model);
        return wrapObject(result, null);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Cinema> findAll(
            @RequestParam(required = false, name = "city") String city,
            @RequestParam(required = false, name = "owner") String owner) throws FSNotFoundException {
        List<Cinema> result = service.findAll(city, owner);
        return wrapList(result, null);
    }

    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Cinema> findOne(@PathVariable String id) throws FSNotFoundException {
        Cinema result = service.findOne(id);
        return wrapObject(result, null);
    }
}
