package com.felixsu.core.controllers.cinema;

import com.felixsu.springcommon.controller.BaseController;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.core.services.cinema.CinemaDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 12/8/16.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "cinema/details")
public class CinemaDetailControllerImpl extends BaseController<CinemaDetail> implements CinemaDetailController {

    private CinemaDetailService service;

    @Autowired
    public CinemaDetailControllerImpl(CinemaDetailService service) {
        this.service = service;
    }

    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<CinemaDetail> detailsUpdate(
            @PathVariable(name = "id") String cinemaId,
            @RequestBody List<CinemaDetail> details) {
        service.detailsUpdate(cinemaId, details);
        return wrapObject(null, null);
    }
}
