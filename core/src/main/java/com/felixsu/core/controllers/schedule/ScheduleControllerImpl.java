package com.felixsu.core.controllers.schedule;

import com.felixsu.springcommon.controller.BaseController;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.core.services.schedule.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by felixsoewito on 12/2/16.
 */

@RestController
@RequestMapping(path = "schedule")
public class ScheduleControllerImpl
        extends BaseController<Schedule>
        implements ScheduleController {

    private ScheduleService service;

    @Autowired
    public ScheduleControllerImpl(ScheduleService service) {
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Schedule> findAll (
            @RequestParam(required = false, name = "city") String city,
            @RequestParam(required = false, name = "movie") String movieId,
            @RequestParam(required = false, name = "cinema") String cinemaId) throws FSNotFoundException {
        List<Schedule> result = service.findAll(city, movieId, cinemaId);
        return wrapList(result, null);
    }

    @RequestMapping(
            path = "cinema/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Schedule> findScheduleByCinema(@PathVariable(name = "id") String id) throws FSNotFoundException {
        List<Schedule> result = service.findAllScheduleForCinema(id);
        return wrapList(result, null);
    }

    @RequestMapping(
            path = "movie/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Schedule> findScheduleByMovie(@PathVariable(name = "id") String id) throws FSNotFoundException {
        List<Schedule> result = service.findAllScheduleForMovie(id);
        return wrapList(result, null);
    }
}
