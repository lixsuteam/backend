package com.felixsu.core.controllers.cinema;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.CinemaPrice;

import java.util.List;

/**
 * Created on 12/8/16.
 *
 * @author felixsoewito
 */
public interface CinemaPriceController {

    RestResponse<CinemaPrice> detailsUpdate(String cinemaId, List<CinemaPrice> prices);

}
