package com.felixsu.core.controllers.movie;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Movie;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public interface MovieController {

    RestResponse<Movie> create(Movie model);

    RestResponse<Movie> findAll(String status, String genre, String city, String owner) throws FSNotFoundException;

    RestResponse<Movie> findOne(String id) throws FSNotFoundException;

}
