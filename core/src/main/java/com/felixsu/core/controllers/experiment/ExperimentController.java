package com.felixsu.core.controllers.experiment;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.other.Experiment;

/**
 * Created on 1/4/17.
 *
 * @author felixsoewito
 */
public interface ExperimentController {

    RestResponse<Experiment> getUnCompleteObject();

    RestResponse<Experiment> getCompleteObject();

    RestResponse<Experiment> storeObject(Experiment item);

}
