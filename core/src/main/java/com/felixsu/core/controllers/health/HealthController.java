package com.felixsu.core.controllers.health;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.general.Health;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public interface HealthController {
    RestResponse<Health> status();
}
