package com.felixsu.core.controllers.schedule;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Schedule;

/**
 * Created by felixsoewito on 12/2/16.
 */
public interface ScheduleController {

    RestResponse<Schedule> findAll(String city, String movieId, String cinemaId) throws FSNotFoundException;
    RestResponse<Schedule> findScheduleByCinema(String id) throws FSNotFoundException;
    RestResponse<Schedule> findScheduleByMovie(String id) throws FSNotFoundException;

}
