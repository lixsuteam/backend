package com.felixsu.core.controllers.experiment;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.other.Experiment;
import com.felixsu.springcommon.controller.BaseController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 1/4/17.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "experiment")
public class ExperimentControllerImpl extends BaseController<Experiment> implements ExperimentController {

    @RequestMapping(
            path = "/uncomplete",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Experiment> getUnCompleteObject() {
        Experiment result = new Experiment(12L, "lix", 24);
        return wrapObject(result, null);
    }

    @RequestMapping(
            path = {"", "complete"},
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Experiment> getCompleteObject() {
        Experiment result = new Experiment(12L, "lix", "su", "lixsu", 55.33, 24);
        return wrapObject(result, null);
    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Experiment> storeObject(@RequestBody Experiment item) {
        return wrapObject(item, null);
    }
}
