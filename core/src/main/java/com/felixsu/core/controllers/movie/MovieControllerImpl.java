package com.felixsu.core.controllers.movie;

import com.felixsu.springcommon.controller.BaseController;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.core.services.movie.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 11/17/16.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping(path = "movie")
public class MovieControllerImpl extends BaseController<Movie> implements MovieController {

    private MovieService service;

    @Autowired
    public MovieControllerImpl(MovieService service) {
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Movie> create(@RequestBody() Movie model) {
        Movie result = service.create(model);
        return wrapObject(result, null);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Movie> findAll(
            @RequestParam(required = false, name = "status") String status,
            @RequestParam(required = false, name = "genre") String genre,
            @RequestParam(required = false, name = "city") String city,
            @RequestParam(required = false, name = "owner") String owner) throws FSNotFoundException {
        List<Movie> result = service.findAll(status, genre, city, owner);
        return wrapList(result, null);
    }

    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Movie> findOne(@PathVariable String id) throws FSNotFoundException {
        Movie result = service.findOne(id);
        return wrapObject(result, null);
    }
}
