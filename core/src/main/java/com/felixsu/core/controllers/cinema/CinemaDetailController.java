package com.felixsu.core.controllers.cinema;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.CinemaDetail;

import java.util.List;

/**
 * Created on 12/8/16.
 *
 * @author felixsoewito
 */
public interface CinemaDetailController {

    RestResponse<CinemaDetail> detailsUpdate(String cinemaId, List<CinemaDetail> details);

}
