package com.felixsu.core.controllers.cgv;

import com.felixsu.common.exception.FsMultipleChoicesException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.general.Acknowledge;

/**
 * Created by felixsoewito on 12/2/16.
 */
public interface CgvController {

    RestResponse<Acknowledge> fetchCinemas() throws FsMultipleChoicesException;
    RestResponse<Acknowledge> fetchMovies() throws FsMultipleChoicesException;
}
