package com.felixsu.core;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created on 03/03/18.
 *
 * @author felixsoewito
 */
public class Etc {

    public Observable<String> getData(final String x) {
        return Observable.create(source -> {
            Thread thread = new Thread(() -> {
                long delay = (long)(Math.random()*1000);
                try {
                    if (x.equalsIgnoreCase("error")) {
                        throw new RuntimeException("error error error");
                    }
                    System.out.println("Observable " + x + " running on " + Thread.currentThread());
                    Thread.sleep(delay);
                    source.onNext(x);
                    source.onComplete();
                } catch (Exception err) {
                    source.onError(err);
                }
            });
            thread.start();
        });
    }
}
