package com.felixsu.repository;

import com.felixsu.common.helper.DateHelper;
import com.felixsu.common.model.cinema.Cinema;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientApplication {

    private static final String  CITY_ELEMENTS_KEY = "city";
    private static final String CGV_BASE_URL = "https://www.cgv.id/en/schedule/cinema";

    private static final int MAX_TRY_COUNT = 3;
    private static final int TIMEOUT = 15000;
    private static final int MAX_THREAD = 8;

    public static void main(String[] args) {
        int tryCount = 0;
        boolean success = false;
        String startTime = DateHelper.getIso8601DateFormatterString();

        final List<Cinema> results = new ArrayList<>();
        Document doc = null;

        do {
            try {
                tryCount++;
                doc = Jsoup.connect(CGV_BASE_URL).timeout(TIMEOUT).get();
                success = true;
            } catch (Exception e){

            }
        } while (!success && tryCount <= MAX_TRY_COUNT);

        if (!success || doc == null){
            return;
        }

        //get all <li> with class city
        Elements cityElements = doc.getElementsByClass(CITY_ELEMENTS_KEY);
        ExecutorService service = Executors.newFixedThreadPool(MAX_THREAD);

        for (Element e : cityElements){
            Elements cinemaElements = e.select("ul").first().getElementsByTag("li");

            String cityName = e.select("a").first().text();

            for (Element ce : cinemaElements){

            }
        }
        service.shutdown();
    }
}