package com.felixsu.repository.services.movie;

import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.repository.entity.cinema.MovieEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.repository.dao.MovieDao;
import com.felixsu.repository.entity.cinema.ScheduleEntity;
import com.felixsu.repository.services.base.BaseAuditableServiceImpl;
import com.felixsu.repository.services.base.PredicateBuilder;
import com.felixsu.repository.services.schedule.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 11/16/16.
 *
 * @author felixsoewito
 */

@Service
public class MovieServiceImpl extends BaseAuditableServiceImpl<MovieDao, MovieEntity, Movie, String> implements MovieService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieServiceImpl.class);

    private ScheduleService scheduleService;

    @Autowired
    public MovieServiceImpl(MovieDao dao, ScheduleService scheduleService) {
        super(dao);
        this.scheduleService = scheduleService;
    }

    @Override
    public List<Movie> findAll(final String owner) throws FSNotFoundException {
        return findAll(null, null, null, owner);
    }

    @Override
    public List<Movie> findAll(final Movie.Status status, final String genre, final String city, final String owner)
            throws FSNotFoundException {

        List<Movie> result = new ArrayList<>();

        List<MovieEntity> queryResult = dao.findAll((root, query, cb) -> {
            PredicateBuilder<MovieEntity> predicateBuilder =
                    new PredicateBuilder<>(root, query, cb);
            predicateBuilder.addEqual("status", status != null? status.getValue() : null);
            predicateBuilder.addEqual("genre", genre);
            predicateBuilder.addEqual("owner", owner);
            predicateBuilder.addEqual("active", true);
            return predicateBuilder.getAndPredicate();
        });

        if (queryResult.isEmpty()) {
            throw new FSNotFoundException("movie with requested parameter not found");
        }

        //do filter if status is now playing
        boolean isNowPlaying = ((status != null) && status.equals(Movie.Status.STATUS_NOW_PLAYING));
        boolean isCityValid = (city != null) && !city.isEmpty();
        if (isNowPlaying && isCityValid) {
            for (MovieEntity m : queryResult) {
                try {
                    List<Schedule> schedules = scheduleService.findAll(city, m.getId(), null);
                    if (!schedules.isEmpty()) {
                        result.add(createModel(m));
                    }
                } catch (FSNotFoundException e) {
                    //do nothing
                }
            }
        } else {
            result.addAll(queryResult.stream().map(this::createModel).collect(Collectors.toList()));
        }

        return result;
    }

    @Override
    public void batchUpdate(final List<Movie> movies, final String owner) {

        List<Movie> persistedMovies;

        try {
            persistedMovies = findAll(owner);
        } catch (FSNotFoundException e) {
            LOGGER.warn(e.getMessage(), e);
            persistedMovies = new ArrayList<>();
        }

        List<Movie> outOfDateMovies = new ArrayList<>();
        List<Movie> newMovies = new ArrayList<>();
        List<Movie> updatedMovies = new ArrayList<>();

        for (Movie m : movies) {
            Movie movie = find(persistedMovies, m);
            if (movie != null) {
                patch(movie, m);
                //add object from persisted one
                updatedMovies.add(movie);
            } else {
                newMovies.add(m);
            }
        }

        for (Movie m : persistedMovies) {
            Movie movie = find(movies, m);

            if (movie == null) {
                //add object from persisted one
                outOfDateMovies.add(m);
            }
        }

        //delete
        for (Movie m : outOfDateMovies) {
            try {
                delete(m.getId());
            } catch (FSNotFoundException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        //update
        for (Movie m : updatedMovies) {
            try {
                update(m);
            } catch (FSNotFoundException | FsBadRequestException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        //create
        for (Movie m : newMovies) {
            try {
                create(m);
            } catch (Exception e) {
                LOGGER.error(e.getMessage() + "on movie ID ->" + m.getId() + e);
            }
        }
    }

    @Override
    protected MovieEntity createEntity(Movie model) {
        MovieEntity entity = new MovieEntity();

        entity.setId(model.getId());
        entity.setMovieName(model.getMovieName());
        entity.setMovieDescription(model.getMovieDescription());
        entity.setActors(model.getActors());
        entity.setCoverUrl(model.getCoverUrl());
        entity.setTrailerUrl(model.getTrailerUrl());
        entity.setDirectors(model.getDirectors());
        entity.setDuration(model.getDuration());
        entity.setLanguage(model.getLanguage());
        entity.setRating(model.getRating());
        entity.setGenre(model.getGenre());
        entity.setSubtitle(model.getSubtitle());
        entity.setStatus(model.getStatus());
        entity.setOwner(model.getOwner());

        entity.setCreatedAt(model.getCreatedAt());
        entity.setDeletedAt(model.getDeletedAt());
        entity.setUpdatedAt(model.getUpdatedAt());

        return entity;
    }

    @Override
    protected Movie createModel(MovieEntity entity) {
        Movie movie = new Movie();

        movie.setId(entity.getId());
        movie.setMovieName(entity.getMovieName());
        movie.setMovieDescription(entity.getMovieDescription());
        movie.setActors(entity.getActors());
        movie.setCoverUrl(entity.getCoverUrl());
        movie.setTrailerUrl(entity.getTrailerUrl());
        movie.setDirectors(entity.getDirectors());
        movie.setDuration(entity.getDuration());
        movie.setLanguage(entity.getLanguage());
        movie.setRating(entity.getRating());
        movie.setGenre(entity.getGenre());
        movie.setSubtitle(entity.getSubtitle());
        movie.setStatus(entity.getStatus());
        movie.setOwner(entity.getOwner());

        movie.setCreatedAt(entity.getCreatedAt());
        movie.setDeletedAt(entity.getDeletedAt());
        movie.setUpdatedAt(entity.getUpdatedAt());

        return movie;
    }

    private Movie find(List<Movie> movies, Movie movie) {
        for (Movie m : movies) {
            if (m.equals(movie)) {
                return m;
            }
        }

        return null;
    }

    private void patch(Movie oldMovie, Movie newMovie) {
        oldMovie.setActors(newMovie.getActors());
        oldMovie.setCoverUrl(newMovie.getCoverUrl());
        oldMovie.setDirectors(newMovie.getDirectors());
        oldMovie.setDuration(newMovie.getDuration());
        oldMovie.setGenre(newMovie.getGenre());
        oldMovie.setLanguage(newMovie.getLanguage());
        oldMovie.setMovieDescription(newMovie.getMovieDescription());
        oldMovie.setMovieName(newMovie.getMovieName());
        oldMovie.setRating(newMovie.getRating());
        oldMovie.setStatus(newMovie.getStatus());
        oldMovie.setSubtitle(newMovie.getSubtitle());
        oldMovie.setTrailerUrl(newMovie.getTrailerUrl());
    }
}
