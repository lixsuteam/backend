package com.felixsu.repository.services.schedule;

import com.felixsu.repository.entity.cinema.ScheduleEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.repository.dao.ScheduleDao;
import com.felixsu.repository.services.base.BaseAuditableServiceImpl;
import com.felixsu.repository.services.base.PredicateBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by felixsoewito on 11/16/16.
 * Implementation of
 * @see ScheduleService
 */

@Service
public class ScheduleServiceImpl extends BaseAuditableServiceImpl<ScheduleDao, ScheduleEntity, Schedule, Long> implements ScheduleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleServiceImpl.class);

    @Autowired
    public ScheduleServiceImpl(ScheduleDao dao) {
        super(dao);
    }

    @Override
    public List<Schedule> findScheduleMovie(String id) throws FSNotFoundException {
        List<ScheduleEntity> queryResult = dao.findByMovieId(id);

        if (queryResult.isEmpty()) {
            throw new FSNotFoundException("schedule for movie " + id + " not found");
        }

        return queryResult.stream().map(this::createModel).collect(Collectors.toList());

    }

    @Override
    public List<Schedule> findScheduleCinema(String id) throws FSNotFoundException {
        List<ScheduleEntity> queryResult = dao.findByCinemaId(id);

        if (queryResult.isEmpty()) {
            throw new FSNotFoundException("schedule for cinema " + id + " not found");
        }

        return queryResult.stream().map(this::createModel).collect(Collectors.toList());
    }

    @Override
    public List<Schedule> findAll(String city, String movieId, String cinemaId) throws FSNotFoundException{

        List<ScheduleEntity> queryResult = dao.findAll((root, query, cb) -> {
            PredicateBuilder<ScheduleEntity> predicateBuilder =
                    new PredicateBuilder<>(root, query, cb);
            predicateBuilder.addEqual("city", city);
            predicateBuilder.addEqual("movieId", movieId);
            predicateBuilder.addEqual("cinemaId", cinemaId);
            predicateBuilder.addEqual("active", true);
            return predicateBuilder.getAndPredicate();
        });

        if (queryResult.isEmpty()) {
            throw new FSNotFoundException("schedule with requested parameter not found");
        }

        return queryResult.stream().map(this::createModel).collect(Collectors.toList());
    }

    @Override
    public void updateSchedules(String cinemaId, List<Schedule> schedules) {

        List<Schedule> persistedSchedules;

        try {
            persistedSchedules = findScheduleCinema(cinemaId);
        } catch (FSNotFoundException e) {
            persistedSchedules = new ArrayList<>();
        }

        List<Schedule> outOfDateSchedules = new ArrayList<>();
        List<Schedule> newSchedules = new ArrayList<>();
        List<Schedule> updatedSchedules = new ArrayList<>();

        int n = persistedSchedules.size();
        int i = 0;

        for (Schedule s : schedules) {
            if (i < n) {
                Schedule persistedSchedule = persistedSchedules.get(i);

                persistedSchedule.setMovieId(s.getMovieId());
                persistedSchedule.setCinemaName(s.getCinemaName());
                persistedSchedule.setFormat(s.getFormat());
                persistedSchedule.setPlayingTime(s.getPlayingTime());
                persistedSchedule.setType(s.getType());
                persistedSchedule.setScheduleClass(s.getScheduleClass());
                persistedSchedule.setPrice(s.getPrice());

                updatedSchedules.add(persistedSchedule);
            } else {
                newSchedules.add(s);
            }
            i++;
        }

        LOGGER.info(String.format("updated %d schedule(s)", i));

        //handle extra persisted
        int j = 0;
        if (i < n) {
            while (i < n) {
                outOfDateSchedules.add(persistedSchedules.get(i));
                i++;
                j++;
            }
        }

        LOGGER.info(String.format("deleted %d schedule(s)", j));

        //delete
        for (Schedule s : outOfDateSchedules) {
            try {
                delete(s.getId());
            } catch (FSNotFoundException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        //update
        for (Schedule s : updatedSchedules) {
            try {
                update(s);
            } catch (FSNotFoundException | FsBadRequestException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        //create
        for (Schedule s : newSchedules) {
            try {
                create(s);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    @Override
    protected ScheduleEntity createEntity(Schedule model) {
        ScheduleEntity entity = new ScheduleEntity();

        entity.setId(model.getId());
        entity.setCinemaId(model.getCinemaId());
        entity.setCinemaName(model.getCinemaName());
        entity.setMovieId(model.getMovieId());
        entity.setFormat(model.getFormat());
        entity.setPlayingTime(model.getPlayingTime());
        entity.setPrice(model.getPrice());
        entity.setScheduleClass(model.getScheduleClass());
        entity.setType(model.getType());
        entity.setCity(model.getCity());

        entity.setCreatedAt(model.getCreatedAt());
        entity.setDeletedAt(model.getDeletedAt());
        entity.setUpdatedAt(model.getUpdatedAt());

        return entity;
    }

    @Override
    protected Schedule createModel(ScheduleEntity entity) {
        Schedule schedule = new Schedule();

        schedule.setId(entity.getId());
        schedule.setCinemaId(entity.getCinemaId());
        schedule.setCinemaName(entity.getCinemaName());
        schedule.setMovieId(entity.getMovieId());
        schedule.setFormat(entity.getFormat());
        schedule.setPlayingTime(entity.getPlayingTime());
        schedule.setPrice(entity.getPrice());
        schedule.setScheduleClass(entity.getScheduleClass());
        schedule.setType(entity.getType());
        schedule.setCity(entity.getCity());

        schedule.setCreatedAt(entity.getCreatedAt());
        schedule.setDeletedAt(entity.getDeletedAt());
        schedule.setUpdatedAt(entity.getUpdatedAt());

        return schedule;
    }
}
