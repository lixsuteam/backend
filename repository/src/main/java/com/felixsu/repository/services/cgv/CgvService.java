package com.felixsu.repository.services.cgv;

import com.felixsu.common.exception.FsMultipleChoicesException;
import com.felixsu.common.model.general.Acknowledge;

/**
 * Created by felixsoewito on 11/28/16.
 */
public interface CgvService {

    Acknowledge fetchCinemasAndSchedules() throws FsMultipleChoicesException;
    Acknowledge fetchMovies() throws FsMultipleChoicesException;
}
