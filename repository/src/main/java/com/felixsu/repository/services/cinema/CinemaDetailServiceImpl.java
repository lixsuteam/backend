package com.felixsu.repository.services.cinema;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.repository.dao.CinemaDetailDao;
import com.felixsu.repository.entity.cinema.CinemaDetailEntity;
import com.felixsu.repository.services.base.BaseAuditableServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by felixsu on 04/12/2016.
 */

@Service
public class CinemaDetailServiceImpl
        extends BaseAuditableServiceImpl<CinemaDetailDao, CinemaDetailEntity, CinemaDetail, Long>
        implements CinemaDetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CinemaDetailServiceImpl.class);

    @Autowired
    public CinemaDetailServiceImpl(CinemaDetailDao dao) {
        super(dao);
    }

    @Override
    public void detailsUpdate(String cinemaId, List<CinemaDetail> cinemaDetails) {
        List<CinemaDetailEntity> persistedEntities = dao.findByCinemaId(cinemaId);

        List<CinemaDetail> persistedDetails = persistedEntities
                .stream()
                .map(this::createModel)
                .collect(Collectors.toList());

        List<CinemaDetail> outOfDatePrices = new ArrayList<>();
        List<CinemaDetail> newPrices = new ArrayList<>();

        for (CinemaDetail cd : persistedDetails) {
            outOfDatePrices.add(cd);
        }

        for (CinemaDetail cd : cinemaDetails) {
            cd.setCinemaId(cinemaId);
            newPrices.add(cd);
        }

        //delete
        for (CinemaDetail cd : outOfDatePrices) {
            try {
                delete(cd.getId());
            } catch (FSNotFoundException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        //create
        for (CinemaDetail cd : newPrices) {
            try {
                create(cd);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public CinemaDetail toModel(CinemaDetailEntity cinemaDetailEntity) {
        return createModel(cinemaDetailEntity);
    }

    @Override
    protected CinemaDetailEntity createEntity(CinemaDetail model) {
        CinemaDetailEntity entity = new CinemaDetailEntity();

        entity.setId(model.getId());
        entity.setCinemaId(model.getCinemaId());
        entity.setType(model.getType());
        entity.setValue(model.getValue());

        entity.setCreatedAt(model.getCreatedAt());
        entity.setDeletedAt(model.getDeletedAt());
        entity.setUpdatedAt(model.getUpdatedAt());

        return entity;
    }

    @Override
    protected CinemaDetail createModel(CinemaDetailEntity entity) {
        CinemaDetail cinemaDetail = new CinemaDetail();

        cinemaDetail.setId(entity.getId());
        cinemaDetail.setCinemaId(entity.getCinemaId());
        cinemaDetail.setType(entity.getType());
        cinemaDetail.setValue(entity.getValue());

        cinemaDetail.setCreatedAt(entity.getCreatedAt());
        cinemaDetail.setDeletedAt(entity.getDeletedAt());
        cinemaDetail.setUpdatedAt(entity.getUpdatedAt());

        return cinemaDetail;
    }
}
