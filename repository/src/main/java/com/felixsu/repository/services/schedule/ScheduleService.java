package com.felixsu.repository.services.schedule;

import com.felixsu.repository.entity.cinema.ScheduleEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.repository.services.base.BaseService;

import java.util.List;

/**
 * Created by felixsoewito on 11/16/16.
 */
public interface ScheduleService extends BaseService<ScheduleEntity, Schedule, Long> {

    List<Schedule> findScheduleMovie(String id) throws FSNotFoundException;

    List<Schedule> findScheduleCinema(String id) throws FSNotFoundException;

    List<Schedule> findAll(String city, String movieId, String cinemaId) throws FSNotFoundException;
    
    void updateSchedules(String cinemaId, List<Schedule> schedules);
}
