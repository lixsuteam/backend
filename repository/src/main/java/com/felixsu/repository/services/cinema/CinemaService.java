package com.felixsu.repository.services.cinema;

import com.felixsu.repository.entity.cinema.CinemaEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.repository.services.base.BaseService;

import java.util.List;

/**
 * Created by felixsoewito on 11/16/16.
 */

public interface CinemaService extends BaseService<CinemaEntity, Cinema, String> {

    List<Cinema> findAll(final String owner) throws FSNotFoundException;

    List<Cinema> findAll(final String city, final String owner) throws FSNotFoundException;

    void batchUpdate(final List<Cinema> cinemas, final String owner);

}
