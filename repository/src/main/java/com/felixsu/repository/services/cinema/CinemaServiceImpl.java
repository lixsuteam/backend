package com.felixsu.repository.services.cinema;

import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.repository.entity.cinema.CinemaDetailEntity;
import com.felixsu.repository.entity.cinema.CinemaEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.repository.dao.CinemaDao;
import com.felixsu.repository.entity.cinema.CinemaPriceEntity;
import com.felixsu.repository.services.base.BaseAuditableServiceImpl;
import com.felixsu.repository.services.base.PredicateBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by felixsoewito on 11/16/16.
 */

@Service
public class CinemaServiceImpl  extends BaseAuditableServiceImpl<CinemaDao, CinemaEntity, Cinema, String> implements CinemaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CinemaServiceImpl.class);

    private CinemaDetailService cinemaDetailService;
    private CinemaPriceService cinemaPriceService;

    @Autowired
    public CinemaServiceImpl(CinemaDao dao, CinemaDetailService cinemaDetailService, CinemaPriceService cinemaPriceService) {
        super(dao);
        this.cinemaDetailService = cinemaDetailService;
        this.cinemaPriceService = cinemaPriceService;
    }

    @Override
    public List<Cinema> findAll(final String owner) throws FSNotFoundException {
        return findAll(null, owner);
    }

    @Override
    public List<Cinema> findAll(final String city, final String owner) throws FSNotFoundException {
        List<Cinema> result = new ArrayList<>();

        List<CinemaEntity> queryResult = dao.findAll((root, query, cb) -> {
            PredicateBuilder<CinemaEntity> predicateBuilder =
                    new PredicateBuilder<>(root, query, cb);
            predicateBuilder.addEqual("city", city);
            predicateBuilder.addEqual("owner", owner);
            predicateBuilder.addEqual("active", true);
            return predicateBuilder.getAndPredicate();
        });

        if (queryResult.isEmpty()) {
            throw new FSNotFoundException("cinema with requested parameter not found");
        }

        result.addAll(queryResult.stream().map(this::createModel).collect(Collectors.toList()));

        return result;
    }

    @Override
    public void batchUpdate(final List<Cinema> cinemas, final String owner) {

        List<Cinema> persistedCinemas;

        try {
            persistedCinemas = findAll(owner);
        } catch (FSNotFoundException e) {
            LOGGER.warn(e.getMessage(), e);
            persistedCinemas = new ArrayList<>();
        }

        List<Cinema> outOfDateCinemas = new ArrayList<>();
        List<Cinema> newCinemas = new ArrayList<>();
        List<Cinema> updatedCinemas = new ArrayList<>();

        for (Cinema c : cinemas) {
            Cinema cinema = find(persistedCinemas, c);
            if (cinema != null) {
                patch(cinema, c);
                //add object from persisted one
                updatedCinemas.add(cinema);
            } else {
                newCinemas.add(c);
            }
        }

        for (Cinema c : persistedCinemas) {
            Cinema cinema = find(cinemas, c);

            if (cinema == null) {
                //add object from persisted one
                outOfDateCinemas.add(c);
            }
        }

        //delete
        for (Cinema c : outOfDateCinemas) {
            try {
                delete(c.getId());
            } catch (FSNotFoundException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        //update
        for (Cinema c : updatedCinemas) {
            try {
                update(c);
            } catch (FSNotFoundException | FsBadRequestException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        //create
        for (Cinema c : newCinemas) {
            try {
                create(c);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    @Override
    protected CinemaEntity createEntity(Cinema model) {
        CinemaEntity entity = new CinemaEntity();

        entity.setId(model.getId());
        entity.setCinemaName(model.getCinemaName());
        entity.setCity(model.getCity());
        entity.setCover(model.getCover());
        entity.setOwner(model.getOwner());

        entity.setCreatedAt(model.getCreatedAt());
        entity.setDeletedAt(model.getDeletedAt());
        entity.setUpdatedAt(model.getUpdatedAt());

        return entity;
    }

    @Override
    protected Cinema createModel(CinemaEntity entity) {
        Cinema cinema = new Cinema();

        cinema.setId(entity.getId());
        cinema.setCinemaName(entity.getCinemaName());
        cinema.setCity(entity.getCity());
        cinema.setCover(entity.getCover());
        cinema.setOwner(entity.getOwner());

        List<CinemaDetail> cinemaDetails = new ArrayList<>();
        for (CinemaDetailEntity cd : entity.getCinemaDetails()) {
            cinemaDetails.add(cinemaDetailService.toModel(cd));
        }

        List<CinemaPrice> cinemaPrices = new ArrayList<>();
        for (CinemaPriceEntity cp : entity.getCinemaPrices()) {
            cinemaPrices.add(cinemaPriceService.toModel(cp));
        }

        cinema.setCinemaDetails(cinemaDetails);
        cinema.setCinemaPrices(cinemaPrices);

        cinema.setCreatedAt(entity.getCreatedAt());
        cinema.setDeletedAt(entity.getDeletedAt());
        cinema.setUpdatedAt(entity.getUpdatedAt());

        return cinema;
    }

    private Cinema find(List<Cinema> cinemas, Cinema cinema) {
        for (Cinema c : cinemas) {
            if (c.equals(cinema)) {
                return c;
            }
        }

        return null;
    }

    private void patch(Cinema oldCinema, Cinema newCinema) {

        oldCinema.setCinemaName(newCinema.getCinemaName());
        oldCinema.setCity(newCinema.getCity());
        oldCinema.setCover(newCinema.getCover());

    }
}
