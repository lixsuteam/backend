package com.felixsu.repository.services.base;

import com.felixsu.common.entity.RepositoryEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.EntityModel;

import java.io.Serializable;

/**
 * Created by felixsoewito on 11/17/16.
 * Entity Service Contract
 */
public interface BaseService<E extends RepositoryEntity, M extends EntityModel<ID>, ID extends Serializable> {

    M create(M model) throws RuntimeException;
    M update(M model) throws FSNotFoundException, FsBadRequestException, RuntimeException;
    void delete(ID id) throws FSNotFoundException, RuntimeException;
    M findOne(ID id) throws FSNotFoundException, RuntimeException;
}
