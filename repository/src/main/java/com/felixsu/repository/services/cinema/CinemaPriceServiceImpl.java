package com.felixsu.repository.services.cinema;

import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.repository.dao.CinemaPriceDao;
import com.felixsu.repository.entity.cinema.CinemaPriceEntity;
import com.felixsu.repository.services.base.BaseAuditableServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by felixsu on 04/12/2016.
 */

@Service
public class CinemaPriceServiceImpl
        extends BaseAuditableServiceImpl<CinemaPriceDao, CinemaPriceEntity, CinemaPrice, Long>
        implements CinemaPriceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CinemaPriceServiceImpl.class);

    @Autowired
    public CinemaPriceServiceImpl(CinemaPriceDao dao) {
        super(dao);
    }

    @Override
    public void pricesUpdate(String cinemaId, List<CinemaPrice> cinemaPrices) {
        List<CinemaPriceEntity> persistedEntities = dao.findByCinemaId(cinemaId);

        List<CinemaPrice> persistedPrices = persistedEntities
                .stream()
                .map(this::createModel)
                .collect(Collectors.toList());

        List<CinemaPrice> outOfDatePrices = new ArrayList<>();
        List<CinemaPrice> newPrices = new ArrayList<>();

        for (CinemaPrice cp : persistedPrices) {
            outOfDatePrices.add(cp);
        }

        for (CinemaPrice cp : cinemaPrices) {
            cp.setCinemaId(cinemaId);
            newPrices.add(cp);
        }

        //delete
        for (CinemaPrice cp : outOfDatePrices) {
            try {
                delete(cp.getId());
            } catch (FSNotFoundException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        //create
        for (CinemaPrice cp : newPrices) {
            try {
                create(cp);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public CinemaPrice toModel(CinemaPriceEntity entity) {
        return createModel(entity);
    }

    @Override
    protected CinemaPriceEntity createEntity(CinemaPrice model) {
        CinemaPriceEntity entity = new CinemaPriceEntity();

        entity.setId(model.getId());
        entity.setCinemaId(model.getCinemaId());
        entity.setType(model.getType());
        entity.setNormalPrice(model.getNormalPrice());
        entity.setOccasionalPrice(model.getOccasionalPrice());
        entity.setHolidayPrice(model.getHolidayPrice());

        entity.setCreatedAt(model.getCreatedAt());
        entity.setDeletedAt(model.getDeletedAt());
        entity.setUpdatedAt(model.getUpdatedAt());

        return entity;
    }

    @Override
    protected CinemaPrice createModel(CinemaPriceEntity entity) {
        CinemaPrice cinemaPrice = new CinemaPrice();

        cinemaPrice.setId(entity.getId());
        cinemaPrice.setCinemaId(entity.getCinemaId());
        cinemaPrice.setType(entity.getType());
        cinemaPrice.setNormalPrice(entity.getNormalPrice());
        cinemaPrice.setOccasionalPrice(entity.getOccasionalPrice());
        cinemaPrice.setHolidayPrice(entity.getHolidayPrice());

        cinemaPrice.setCreatedAt(entity.getCreatedAt());
        cinemaPrice.setDeletedAt(entity.getDeletedAt());
        cinemaPrice.setUpdatedAt(entity.getUpdatedAt());

        return cinemaPrice;
    }
}
