package com.felixsu.repository.services.cinema;

import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.repository.entity.cinema.CinemaDetailEntity;
import com.felixsu.repository.services.base.BaseService;

import java.util.List;

/**
 * Created by felixsu on 04/12/2016.
 */
public interface CinemaDetailService extends BaseService<CinemaDetailEntity, CinemaDetail, Long> {

    void detailsUpdate(String cinemaId, List<CinemaDetail> cinemaDetails);

    CinemaDetail toModel(CinemaDetailEntity cinemaDetailEntity);

}
