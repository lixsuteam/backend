package com.felixsu.repository.services.base;

import com.felixsu.common.entity.RepositoryEntity;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by felixsoewito on 12/2/16.
 */
public class PredicateBuilder<E extends RepositoryEntity> {
    private List<Predicate> predicates = new ArrayList<>();
    private Root<E> root;
    private CriteriaQuery query;
    private CriteriaBuilder builder;

    public PredicateBuilder(Root<E> root, CriteriaQuery query, CriteriaBuilder cb) {
        this.root = root;
        this.query = query;
        this.builder = cb;
    }

    public void addEqual(String property, Integer value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public void addEqual(String property, String value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public void addEqual(String property, Boolean value) {
        if (value != null) {
            predicates.add(builder.equal(root.get(property), value));
        }
    }

    public Predicate getAndPredicate() {
        Predicate result = builder.and(predicates.toArray(new Predicate[predicates.size()]));
//
//        for (Predicate predicate : predicates) {
//            result = builder.and(predicate);
//        }

        return result;
    }
}
