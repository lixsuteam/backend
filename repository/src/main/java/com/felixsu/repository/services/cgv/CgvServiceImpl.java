package com.felixsu.repository.services.cgv;

import com.felixsu.common.exception.FsMultipleChoicesException;
import com.felixsu.common.model.extra.CgvItem;
import com.felixsu.common.model.general.Acknowledge;
import com.felixsu.repository.client.CgvClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created on 11/28/16.
 *
 * @author felixsoewito
 */

@Service
public class CgvServiceImpl implements CgvService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CgvServiceImpl.class);

    private final CgvClient cgvClient;

    private boolean fetchingCinema = false;
    private boolean fetchingMovie = false;

    @Autowired
    public CgvServiceImpl(CgvClient cgvClient) {
        this.cgvClient = cgvClient;
    }

    @Override
    @Scheduled(cron = "0 0 0 1/1 * ?")
    public Acknowledge fetchCinemasAndSchedules() throws FsMultipleChoicesException {

        if (!fetchingCinema) {

            fetchingCinema = true;

            try {
                cgvClient.fetchCinemaAndSchedule(new CgvClient.OnFinishListener() {
                    @Override
                    public <T extends CgvItem> void onFinish(Throwable e, List<T> results) {
                        if (e == null) {
                            LOGGER.info("success get cinema data, size=> " + results.size());
                        } else {
                            LOGGER.error("error get cinema data", e);
                        }

                        fetchingCinema = false;
                    }
                });
            } catch (Exception e){
                LOGGER.error(e.getMessage(), e);
                fetchingCinema = false;
            }

            return new Acknowledge("OK");
        } else {
            throw new FsMultipleChoicesException("We are working on it, please wait");
        }

    }

    @Override
    @Scheduled(cron = "0 13 0 1/1 * ?")
    public Acknowledge fetchMovies() throws FsMultipleChoicesException{

        if (!fetchingMovie) {

            fetchingMovie = true;

            try {
                cgvClient.fetchMovie(new CgvClient.OnFinishListener() {
                    @Override
                    public <T extends CgvItem> void onFinish(Throwable e, List<T> results) {
                        if (e == null) {
                            LOGGER.info("success get movie data, size=> " + results.size());
                        } else {
                            LOGGER.error("error get movie data", e);
                        }

                        fetchingMovie = false;
                    }
                });
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                fetchingMovie = false;
            }

            return new Acknowledge("OK");
        } else {
            throw new FsMultipleChoicesException("We are working on it, please wait");
        }

    }
}
