package com.felixsu.repository.services.movie;

import com.felixsu.repository.entity.cinema.MovieEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.repository.services.base.BaseService;

import java.util.List;

/**
 * Created by felixsoewito on 11/16/16.
 */
public interface MovieService extends BaseService<MovieEntity, Movie, String> {

    List<Movie> findAll(final String owner) throws FSNotFoundException;

    List<Movie> findAll(final Movie.Status status, final String genre, final String city, final String owner)
            throws FSNotFoundException;

    void batchUpdate(final List<Movie> movies, final String owner);

}
