package com.felixsu.repository.services.base;

import com.felixsu.common.entity.RepositoryEntity;
import com.felixsu.common.model.EntityModel;
import com.felixsu.repository.dao.BaseDao;

import java.io.Serializable;

/**
 * Created by felixsoewito on 11/17/16.
 */
public abstract class BaseServiceImpl<D extends BaseDao<E, ID>, E extends RepositoryEntity, M extends EntityModel<ID>, ID extends Serializable> implements BaseService<E,M,ID> {

    private D dao;

    public BaseServiceImpl(D dao) {
        this.dao = dao;
    }
}
