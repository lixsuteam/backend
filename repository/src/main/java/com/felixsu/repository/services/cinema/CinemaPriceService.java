package com.felixsu.repository.services.cinema;

import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.repository.entity.cinema.CinemaPriceEntity;
import com.felixsu.repository.services.base.BaseService;

import java.util.List;

/**
 * Created by felixsu on 04/12/2016.
 */
public interface CinemaPriceService extends BaseService<CinemaPriceEntity, CinemaPrice, Long> {

    void pricesUpdate(String cinemaId, List<CinemaPrice> cinemaPrices);

    CinemaPrice toModel(CinemaPriceEntity entity);

}
