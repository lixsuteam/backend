package com.felixsu.repository.services.base;

import com.felixsu.repository.entity.AuditableEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.EntityModel;
import com.felixsu.repository.dao.BaseDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * Created by felixsoewito on 11/17/16.
 */
public abstract class BaseAuditableServiceImpl<D extends BaseDao<E, ID>, E extends AuditableEntity, M extends EntityModel<ID>, ID extends Serializable>
        implements BaseService<E, M, ID> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseAuditableServiceImpl.class);

    protected D dao;

    public BaseAuditableServiceImpl(D dao) {
        this.dao = dao;
    }

    protected abstract E createEntity(M model);
    protected abstract M createModel(E entity);

    @Override
    public M create(M model) throws RuntimeException {
        M result;

        boolean isSoftDeleted = false;

        try {
            if (model.getId() != null) {
                isSoftDeleted = dao.exists(model.getId());
            }

            if (isSoftDeleted) {
                E entity = dao.findOne(model.getId());
                M persistedModel = createModel(entity);
                result = update(persistedModel);
            } else {
                E entity = createEntity(model);
                entity.create();
                E entityResult = dao.save(entity);
                result = createModel(entityResult);
            }

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }

        return result;
    }

    @Override
    public M update(M model) throws FSNotFoundException, FsBadRequestException, RuntimeException {
        M result;

        try {
            E entity = createEntity(model);

            if (dao.exists(model.getId())) {
                E persistedEntity = dao.findOne(model.getId());
                validateUpdate(persistedEntity, entity);

                E entityResult = dao.save(entity);
                result = createModel(entityResult);
            } else {
                throw new FSNotFoundException("update fail! Entity with ID " + model.getId() + " does not exist");
            }
        } catch (FSNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            throw new FSNotFoundException(e.getMessage());
        } catch (FsBadRequestException e) {
            LOGGER.error(e.getMessage(), e);
            throw new FsBadRequestException(e.getMessage());
        }catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }

        return result;
    }

    @Override
    public void delete(ID id) throws FSNotFoundException, RuntimeException {

        try {
            if (dao.exists(id)) {
                E persistedEntity = dao.findOne(id);

                persistedEntity.delete();
                dao.save(persistedEntity);
            } else {
                throw new FSNotFoundException("delete fail! Entity with ID " + id + " does not exist");
            }
        } catch (FSNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            throw new FSNotFoundException(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public M findOne(ID id) throws FSNotFoundException, RuntimeException {
        M result;

        try {
            E persistedEntity = dao.findOneActive(id);

            if (persistedEntity != null) {
                result = createModel(persistedEntity);
            } else {
                throw new FSNotFoundException("findOne fail! Entity with ID " + id + " does not exist");
            }
        } catch (FSNotFoundException e) {
            LOGGER.error(e.getMessage(), e);
            throw new FSNotFoundException(e.getMessage());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage());
        }

        return result;
    }

    private void validateUpdate(E persistedEntity, E newEntity) throws FsBadRequestException {
        if (newEntity.getUpdatedAt() == null) {
            throw new FsBadRequestException("updated at field can not be empty");
        }

        if (newEntity.getCreatedAt() == null) {
            newEntity.setCreatedAt(persistedEntity.getCreatedAt());
        }

        //activate entity
        newEntity.setActive(true);
    }

}
