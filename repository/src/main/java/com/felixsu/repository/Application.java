package com.felixsu.repository;

import com.felixsu.common.model.cinema.*;
import com.felixsu.repository.client.CgvClient;
import com.felixsu.springcommon.client.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@SpringBootApplication
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.felixsu.repository.entity"})
@ComponentScan(basePackages = {
        "com.felixsu.repository",
        "com.felixsu.springcommon.controller.advice",
        "com.felixsu.springcommon.config"})
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        LOGGER.info("Starting REPOSITORY Application");

        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            LOGGER.info(beanName);
        }
    }

    @Bean
    CinemaEntityClient createCinemaEntityClient() {
        return new CinemaEntityClient(new RestTemplate(), "http://localhost:9000/cinema", Cinema.class);
    }

    @Bean
    MovieEntityClient createMovieEntityClient() {
        return new MovieEntityClient(new RestTemplate(), "http://localhost:9000/movie", Movie.class);
    }

    @Bean
    ScheduleEntityClient createScheduleEntityClient() {
        return new ScheduleEntityClient(new RestTemplate(), "http://localhost:9000/schedule", Schedule.class);
    }

    @Bean
    CinemaDetailClient createCinemaDetailClient() {
        return new CinemaDetailClient(new RestTemplate(), "http://localhost:9000/cinema/details", CinemaDetail.class);
    }

    @Bean
    CinemaPriceClient createCinemaPriceClient() {
        return new CinemaPriceClient(new RestTemplate(), "http://localhost:9000/cinema/prices", CinemaPrice.class);
    }

    @Bean
    CgvClient createCgvClient() {
        return new CgvClient(createCinemaEntityClient(),
                createScheduleEntityClient(), createMovieEntityClient());
    }


}