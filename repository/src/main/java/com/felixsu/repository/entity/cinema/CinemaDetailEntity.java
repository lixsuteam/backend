package com.felixsu.repository.entity.cinema;

import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.repository.entity.AuditableEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by felixsoewito on 11/19/16.
 */

@Entity
@Table(name = "cinema_detail")
public class CinemaDetailEntity extends AuditableEntity implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "cinema_id")
    private String cinemaId;

    @Column(name = "type")
    @Enumerated(value = EnumType.ORDINAL)
    private CinemaDetail.DetailType type;

    @Column(name = "value")
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(String cinemaId) {
        this.cinemaId = cinemaId;
    }

    public CinemaDetail.DetailType getType() {
        return type;
    }

    public void setType(CinemaDetail.DetailType type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
