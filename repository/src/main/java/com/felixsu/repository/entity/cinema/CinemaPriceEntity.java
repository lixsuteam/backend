package com.felixsu.repository.entity.cinema;

import com.felixsu.repository.entity.AuditableEntity;
import com.felixsu.common.model.cinema.CinemaPrice;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by felixsoewito on 11/22/16.
 */

@Entity
@Table(name = "cinema_price")
public class CinemaPriceEntity extends AuditableEntity implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "cinema_id")
    private String cinemaId;

    @Column(name = "holiday")
    private String holidayPrice;

    @Column(name = "normal")
    private String normalPrice;

    @Column(name = "occasional")
    private String occasionalPrice;

    @Column(name = "type")
    @Enumerated(value = EnumType.ORDINAL)
    private CinemaPrice.PriceType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(String cinemaId) {
        this.cinemaId = cinemaId;
    }

    public String getHolidayPrice() {
        return holidayPrice;
    }

    public void setHolidayPrice(String holidayPrice) {
        this.holidayPrice = holidayPrice;
    }

    public String getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(String normalPrice) {
        this.normalPrice = normalPrice;
    }

    public String getOccasionalPrice() {
        return occasionalPrice;
    }

    public void setOccasionalPrice(String occasionalPrice) {
        this.occasionalPrice = occasionalPrice;
    }

    public CinemaPrice.PriceType getType() {
        return type;
    }

    public void setType(CinemaPrice.PriceType type) {
        this.type = type;
    }
}
