package com.felixsu.repository.entity.cinema;

import com.felixsu.repository.entity.AuditableEntity;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by felixsoewito on 11/10/16.
 */

@Entity
@Table(name = "cinema")
public class CinemaEntity extends AuditableEntity implements Serializable{

    @Id
    private String id;

    @Column(name = "cinema_name")
    private String cinemaName;

    @Column(name = "city")
    private String city;

    @Column(name = "cover")
    private String cover;

    @Column(name = "owner")
    private String owner;

    @OneToMany(mappedBy="cinemaId")
    @Where(clause="is_active = true")
    private List<CinemaDetailEntity> cinemaDetails = new ArrayList<>();

    @OneToMany(mappedBy="cinemaId")
    @Where(clause="is_active = true")
    private List<CinemaPriceEntity> cinemaPrices = new ArrayList<>();

    public CinemaEntity() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public List<CinemaDetailEntity> getCinemaDetails() {
        return cinemaDetails;
    }

    public void setCinemaDetails(List<CinemaDetailEntity> cinemaDetails) {
        this.cinemaDetails = cinemaDetails;
    }

    public List<CinemaPriceEntity> getCinemaPrices() {
        return cinemaPrices;
    }

    public void setCinemaPrices(List<CinemaPriceEntity> cinemaPrices) {
        this.cinemaPrices = cinemaPrices;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
