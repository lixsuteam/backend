package com.felixsu.repository.entity.cinema;

import com.felixsu.repository.entity.AuditableEntity;
import com.felixsu.common.model.cinema.Movie;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by felixsoewito on 11/15/16.
 */

@Entity
@Table(name = "movie")
public class MovieEntity extends AuditableEntity implements Serializable {

    @Id
    private String id;

    @Column(name = "movie_name")
    private String movieName;

    @Column(name = "movie_description")
    private String movieDescription;

    @Column(name = "language")
    private String language;

    @Column(name = "genre")
    private String genre;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "directors")
    private String directors;

    @Column(name = "cover_url")
    private String coverUrl;

    @Column(name = "actors")
    private String actors;

    @Column(name = "rating")
    private String rating;

    @Column(name = "status")
    @Enumerated(value = EnumType.ORDINAL)
    private Movie.Status status;

    @Column(name = "subtitle")
    private String subtitle;

    @Column(name = "trailer_url")
    private String trailerUrl;

    @Column(name = "owner")
    private String owner;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Movie.Status getStatus() {
        return status;
    }

    public void setStatus(Movie.Status status) {
        this.status = status;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
