package com.felixsu.repository.entity.cinema;


import com.felixsu.repository.entity.AuditableEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by felixsoewito on 11/15/16.
 */

@Entity
@Table(name = "schedule")
public class ScheduleEntity extends AuditableEntity implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "cinema_id")
    private String cinemaId;

    @Column(name = "cinema_name")
    private String cinemaName;

    @Column(name = "movie_id")
    private String movieId;

    @Column(name = "class")
    private String scheduleClass;

    @Column(name = "format")
    private String format;

    @Column(name = "playing_time")
    private String playingTime;

    @Column(name = "price")
    private String price;

    @Column(name = "type")
    private String type;

    @Column(name = "city")
    private String city;

    public ScheduleEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(String cinemaId) {
        this.cinemaId = cinemaId;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getScheduleClass() {
        return scheduleClass;
    }

    public void setScheduleClass(String scheduleClass) {
        this.scheduleClass = scheduleClass;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(String playingTime) {
        this.playingTime = playingTime;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }
}
