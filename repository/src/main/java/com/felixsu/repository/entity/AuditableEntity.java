package com.felixsu.repository.entity;

import com.felixsu.common.entity.RepositoryEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by felixsoewito on 11/13/16.
 */

@MappedSuperclass
public class AuditableEntity implements Serializable, RepositoryEntity {

    @Column(name = "is_active")
    private Boolean active;

    @Column(name = "created_at")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createdAt;

    @Version
    @Column(name = "updated_at")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date updatedAt;

    @Column(name = "deleted_at")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date deletedAt;

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Date deletedAt) {
        this.deletedAt = deletedAt;
    }

    public void create() {
        this.createdAt = new Date();
        setActive(true);
    }

    public void delete() {
        this.deletedAt = new Date();
        setActive(false);
    }


}
