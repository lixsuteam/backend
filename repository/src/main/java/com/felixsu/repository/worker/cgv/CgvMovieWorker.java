package com.felixsu.repository.worker.cgv;

import com.felixsu.common.helper.DateHelper;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.repository.client.CgvClient;
import com.felixsu.repository.worker.MovieWorker;
import com.felixsu.springcommon.client.entity.MovieEntityClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 11/24/16.
 *
 * @author felixsoewito
 */
public class CgvMovieWorker extends BaseExecutorWorker implements MovieWorker {

    private static final Logger LOGGER = LoggerFactory.getLogger(CgvMovieWorker.class);

    private static final int MAX_TRY_COUNT = 3;
    private static final int TIMEOUT = 15000;
    private static final String BASE_URL = "https://www.cgv.id";
    private static final String MOVIE_URL = BASE_URL + "/en/movies/now_playing";

    private final CgvClient.OnFinishListener listener;
    private final MovieEntityClient client;

    public CgvMovieWorker(CgvClient.OnFinishListener listener, MovieEntityClient client) {
        this.listener = listener;
        this.client = client;
    }

    @Override
    public void run() {
        String startTime = DateHelper.getIso8601DateFormatterString();
        LOGGER.debug("Movie worker start");

        List<Movie> results = new ArrayList<>();
        Document doc = null;
        boolean success = false;
        int tryCount = 0;
        do {
            tryCount++;
            try {
                doc = Jsoup.connect(MOVIE_URL).timeout(TIMEOUT).get();
                success = true;
            } catch (Exception e){
                LOGGER.info("error call movie url on " + startTime + " with counter= "  + tryCount);
                if (tryCount > MAX_TRY_COUNT){
                    LOGGER.warn("reach max try call movie url on " + startTime + "with counter= " + tryCount, e);
                    listener.onFinish(e, null);
                }
            }
        } while (!success && tryCount <= MAX_TRY_COUNT);

        if (!success || doc == null){
            return;
        }

        final Consumer<Element> nowPlayingUrlConsumer = e -> {
            String url = BASE_URL + e.attr("href");
            try {
                Movie m = readMovie(url, true);
                if (m != null) {
                    LOGGER.info("adding to movie results => ID:" + m.getId());
                    results.add(m);
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        };

        final Consumer<Element> comingSoonUrlConsumer = e -> {
            String url = BASE_URL + e.attr("href");
            try {
                Movie m = readMovie(url, false);
                if ((m != null) && (find(results, m) == null)) {
                    LOGGER.info("adding to movie results => ID:" + m.getId());
                    results.add(m);
                } else {
                    if (m != null) {
                        LOGGER.warn("Duplicate now playing and coming soon movies found => ID:" + m.getId());
                    } else {
                        LOGGER.warn("movie null on coming soon section");
                    }
                }
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        };

        doc
                .body()
                .getElementsByClass("movie-list-body").get(0)
                .getElementsByTag("a")
                .parallelStream()
                .forEach(nowPlayingUrlConsumer);
        doc
                .body()
                .getElementsByClass("comingsoon-movie-list-body").get(0)
                .getElementsByTag("a")
                .parallelStream()
                .forEach(comingSoonUrlConsumer);

        try {
            client.batchUpdate(results, Cinema.CGV);
        } catch (Exception e) {
            LOGGER.error("failed to finalize cinema data!", e);
        }
        listener.onFinish(null, results);
    }

    private Movie find(List<Movie> movies, Movie movie) {
        for (Movie m : movies) {
            if (m.equals(movie)) {
                return m;
            }
        }
        return null;
    }

    private Movie readMovie(String url, boolean isNowPlaying) throws IOException {
        String startTime = DateHelper.getIso8601DateFormatterString();
        Document doc = null;
        boolean success = false;
        int tryCount = 0;
        do {
            tryCount++;
            try {
                doc = Jsoup.connect(url)
                        .timeout(TIMEOUT)
                        .get();
                success = true;
            } catch (Exception e){
                LOGGER.info("error call movie url on " + startTime + " with counter= "  + tryCount);
                if (tryCount > MAX_TRY_COUNT){
                    LOGGER.warn("reach max try call movie url on " + startTime + "with counter= " + tryCount, e);
                }
            }
        } while (!success && tryCount <= MAX_TRY_COUNT);

        if (!success || doc == null){
            return null;
        }

        Document movieDoc = Jsoup.connect(url)
                .timeout(10000)
                .get();

        Movie movie = new Movie();
        try {
            String[] urlComponents = url.split("/");
            movie.setId(urlComponents[urlComponents.length-1]);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException("Failed to read movie ID");
        }

        if (isNowPlaying) {
            movie.setStatus(Movie.Status.STATUS_NOW_PLAYING);
        } else {
            movie.setStatus(Movie.Status.STATUS_COMING_SOON);
        }

        movie.setOwner(Cinema.CGV);

        setCoverUrl(movieDoc, movie);
        setTrailerUrl(movieDoc, movie);
        setMovieTitle(movieDoc, movie);
        setMovieSynopsis(movieDoc, movie);
        setMovieProperties(movieDoc, movie);
        LOGGER.info("read movie success => " + movie.getId() + "-" + movie.getStatus());
        return movie;
    }

    private void setCoverUrl(Document movieDoc, Movie movie) {
        try {
            String coverUrl = movieDoc
                    .getElementsByClass("poster-section").get(0)
                    .getElementsByTag("img").get(0)
                    .attr("src");
            movie.setCoverUrl(coverUrl);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            movie.setCoverUrl("ERROR");
        }
    }

    private void setTrailerUrl(Document movieDoc, Movie movie) {
        try {
            String trailerUrl = movieDoc
                    .getElementsByClass("trailer-section").get(0)
                    .getElementsByTag("iframe").get(0)
                    .attr("src");
            movie.setTrailerUrl(trailerUrl);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            movie.setTrailerUrl("ERROR");
        }
    }

    private void setMovieSynopsis(Document movieDoc, Movie movie) {
        try {
            List<String> synopsis =  movieDoc
                    .getElementsByClass("movie-synopsis").get(0)
                    .textNodes()
                    .stream()
                    .map(TextNode::text)
                    .filter(s -> !s.trim().isEmpty())
                    .collect(Collectors.toList());
            movie.setMovieDescription(synopsis.get(synopsis.size()-1));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            movie.setMovieDescription("ERROR");
        }
    }

    private void setMovieTitle(Document movieDoc, Movie movie) {
        try {
            final String title = movieDoc
                    .getElementsByClass("movie-info-title").get(0)
                    .text();
            movie.setMovieName(title);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            movie.setMovieName("ERROR");
        }
    }

    private void setMovieProperties(Document movieDoc, Movie movie) {
        try {
            List<Element> elements = movieDoc
                    .getElementsByClass("movie-add-info").get(0)
                    .getElementsByTag("li");

            for (int i = 0; i < elements.size(); i++) {
                try {
                    String text = elements.get(i).text();
                    if (text.contains("STARRING")) {
                        String [] actors = text
                                .split(":")[1].trim()
                                .split(",");
                        movie.setActors(Stream.of(actors).collect(Collectors.joining("#")));
                    }
                    if (text.contains("DIRECTOR")) {
                        movie.setDirectors(text.split(":")[1].trim());
                    }
                    if (text.contains("CENSOR")) {
                        movie.setRating(text.split(":")[1].trim());
                    }
                    if (text.contains("GENRE")) {
                        movie.setGenre(text.split(":")[1].trim());
                    }
                    if (text.contains("LANGUAGE")) {
                        movie.setLanguage(text.split(":")[1].trim());
                    }
                    if (text.contains("SUBTITLE")) {
                        movie.setSubtitle(text.split(":")[1].trim());
                    }
                    if (text.contains("DURATION")) {
                        int duration = 0;
                        try {
                            duration = Integer.valueOf(text
                                    .split(":")[1].trim()
                                    .split(" ")[0].trim());
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        movie.setDuration(duration);
                    }
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }


}
