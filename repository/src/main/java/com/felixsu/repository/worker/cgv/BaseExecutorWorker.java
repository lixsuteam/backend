package com.felixsu.repository.worker.cgv;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created on 11/24/16.
 *
 * @author felixsoewito
 */
public class BaseExecutorWorker extends Thread {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseExecutorWorker.class);

    protected void shutdownExecutor(ExecutorService service) {
        try {
            if (!service.awaitTermination(360, TimeUnit.SECONDS)){
                service.shutdownNow();
            }
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
            service.shutdownNow();
        }
    }
}
