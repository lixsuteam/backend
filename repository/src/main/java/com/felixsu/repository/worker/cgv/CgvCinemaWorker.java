package com.felixsu.repository.worker.cgv;

import com.felixsu.common.helper.DateHelper;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.repository.client.CgvClient;
import com.felixsu.repository.worker.CinemaWorker;
import com.felixsu.springcommon.client.entity.CinemaEntityClient;
import com.felixsu.springcommon.client.entity.ScheduleEntityClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 11/24/16.
 *
 * @author felixsoewito
 */
public class CgvCinemaWorker extends BaseExecutorWorker implements CinemaWorker {

    private static final Logger LOGGER = LoggerFactory.getLogger(CinemaWorker.class);

    static final String BASE_URL = "https://www.cgv.id";
    private final String CGV_BASE_URL = BASE_URL + "/en/schedule/cinema";

    private final int MAX_TRY_COUNT = 5;
    private final int TIMEOUT = 15000;

    private final CgvClient.OnFinishListener listener;
    private final CinemaEntityClient cinemaEntityClient;
    private final ScheduleEntityClient scheduleEntityClient;

    public CgvCinemaWorker(CgvClient.OnFinishListener listener,
                           CinemaEntityClient cinemaEntityClient,
                           ScheduleEntityClient scheduleEntityClient) {
        this.listener = listener;
        this.cinemaEntityClient = cinemaEntityClient;
        this.scheduleEntityClient = scheduleEntityClient;
    }

    @Override
    public void run() {
        final String startTime = DateHelper.getIso8601DateFormatterString();
        final List<Cinema> results = Collections.synchronizedList(new ArrayList<>());

        LOGGER.debug("Cinema worker start");
        Document doc = null;
        int tryCount = 0;
        boolean success = false;
        do {
            try {
                tryCount++;
                doc = Jsoup.connect(CGV_BASE_URL).timeout(TIMEOUT).get();
                success = true;
            } catch (Exception e) {
                LOGGER.info("error call base cinema url: " + CGV_BASE_URL + " on " + startTime + "with counter=" + tryCount);
                if (tryCount == MAX_TRY_COUNT) {
                    LOGGER.warn("reach max try call cinema url on $startTime with counter= $tryCount", e);
                    listener.onFinish(e, null);
                }
            }
        } while (!success && tryCount <= MAX_TRY_COUNT);

        if (!success || doc == null) {
            return;
        }

        List<Element> citiesElements = new ArrayList<>();
        citiesElements.addAll(doc.body().getElementsByClass("city on"));
        citiesElements.addAll(doc.body().getElementsByClass("city"));
        LOGGER.info("cities elements: " + citiesElements.size());

        citiesElements.forEach(new CinemaConsumer(results, scheduleEntityClient));

        results.sort((o1, o2) -> {
            int id1 = Integer.valueOf(o1.getId());
            int id2 = Integer.valueOf(o2.getId());
            return id1 - id2;
        });

        try {
            cinemaEntityClient.batchUpdate(results, Cinema.CGV);
        } catch (Exception e) {
            LOGGER.error("failed to finalize cinema data!", e);
        }
        listener.onFinish(null, results);
    }

    public static class CinemaConsumer implements Consumer<Element> {

        private final List<Cinema> results;
        private final ScheduleEntityClient scheduleEntityClient;

        public CinemaConsumer(List<Cinema> results, ScheduleEntityClient scheduleEntityClient) {
            this.results = results;
            this.scheduleEntityClient = scheduleEntityClient;
        }

        @Override
        public void accept(Element e) {
            final String city = e.getElementsByTag("a").get(0).text();
            List<Cinema> datas = e.getElementsByTag("a")
                    .stream()
                    .filter(e1 -> e1.hasClass("cinema_fav"))
                    .map(e1 -> {
                        String id = e1.attr("id");
                        String cinemaName = e1.attr("title");
                        String url = BASE_URL + e1.attr("href");

                        Cinema cinema = new Cinema();
                        cinema.setId(id);
                        cinema.setOwner(Cinema.CGV);
                        cinema.setCinemaName(cinemaName);
                        cinema.setCity(city);

                        readCompleteCinemaData(url, cinema);

                        return cinema;
                    })
                    .collect(Collectors.toList());
            results.addAll(datas);
        }

        private void readCompleteCinemaData(String cinemaUrl, final Cinema cinema) {
            try {
                Document doc = Jsoup.connect(cinemaUrl)
                        .timeout(10000)
                        .get();

                final String cinemaCover = doc.body()
                        .getElementsByClass("cinema-info").get(0)
                        .getElementsByTag("img").get(0)
                        .attr("src");
                cinema.setCover(cinemaCover);

                List<Schedule> schedules = new ArrayList<>();
                doc.body()
                        .getElementsByClass("schedule-lists").get(0)
                        .getElementsByClass("schedule-title")
                        .forEach(element -> {
//                            final String movieName = element
//                                    .getElementsByTag("a").get(0)
//                                    .text().trim();
                            final String movieId = element
                                    .getElementsByTag("a").get(0)
                                    .attr("href")
                                    .split("/")[4];
//                            final String[] scheduleDetails = element
//                                    .getElementsByTag("span").get(0)
//                                    .text().split("/");

                            element.nextElementSibling()
                                    .getElementsByClass("schedule-type")
                                    .forEach(element2 -> {
                                        final Schedule schedule = new Schedule();
                                        schedule.setMovieId(movieId);
                                        schedule.setCinemaId(cinema.getId());
                                        schedule.setCinemaName(cinema.getCinemaName());
                                        schedule.setCity(cinema.getCity());

                                        schedule.setType("NA");
                                        schedule.setPrice("NA");
                                        schedule.setScheduleClass(element2
                                                .getElementsByClass("audi-nm").get(0)
                                                .text().split("#")[0].trim());
                                        schedule.setFormat(element2.ownText().trim());

                                        String playingTime = element2.nextElementSibling().getElementsByTag("a")
                                                .stream()
                                                .map(timeElement -> timeElement.ownText().trim())
                                                .collect(Collectors.joining("#"));
                                        schedule.setPlayingTime(playingTime);
                                        schedules.add(schedule);
                                    });
                        });
                scheduleEntityClient.updateSchedules(cinema.getId(), schedules);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }
}
