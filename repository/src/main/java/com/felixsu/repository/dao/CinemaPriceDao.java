package com.felixsu.repository.dao;

import com.felixsu.repository.entity.cinema.CinemaPriceEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by felixsu on 04/12/2016.
 * repository query method for cinema_price table
 */

@Repository
public interface CinemaPriceDao extends BaseDao<CinemaPriceEntity, Long> {

    @Query("select e from CinemaPriceEntity e where e.active=true and e.cinemaId = :cinemaId")
    List<CinemaPriceEntity> findByCinemaId(@Param("cinemaId") String cinemaId);
}
