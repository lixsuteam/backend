package com.felixsu.repository.dao;

import com.felixsu.common.entity.RepositoryEntity;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.io.Serializable;
import java.util.List;

/**
 * Created by felixsoewito on 11/13/16.
 */

@NoRepositoryBean
public interface BaseDao<E extends RepositoryEntity, ID extends Serializable> extends CrudRepository<E, ID> {

    @Query("select e from #{#entityName} e where e.active=true and e.id=:id")
    E findOneActive(@Param("id") ID id);

    @Override
    @Query("select e from #{#entityName} e where e.active=true")
    Iterable<E> findAll();

    List<E> findAll(Specification<E> specification);
}
