package com.felixsu.repository.dao;

import com.felixsu.repository.entity.cinema.MovieEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by felixsoewito on 11/15/16.
 */

@Repository
public interface MovieDao extends BaseDao<MovieEntity, String> {

}
