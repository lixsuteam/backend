package com.felixsu.repository.dao;

import com.felixsu.repository.entity.cinema.ScheduleEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by felixsoewito on 11/15/16.
 */

@Repository
public interface ScheduleDao extends BaseDao<ScheduleEntity, Long> {

    @Query("select e from ScheduleEntity e where e.active=true and e.cinemaId = :cinemaId")
    List<ScheduleEntity> findByCinemaId(@Param("cinemaId") String cinemaId);

    @Query("select e from ScheduleEntity e where e.active=true and e.movieId = :movieId")
    List<ScheduleEntity> findByMovieId(@Param("movieId") String movieId);

}
