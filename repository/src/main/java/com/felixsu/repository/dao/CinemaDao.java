package com.felixsu.repository.dao;

import com.felixsu.repository.entity.cinema.CinemaEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by felixsoewito on 11/15/16.
 */

@Repository
public interface CinemaDao extends BaseDao<CinemaEntity, String> {
}
