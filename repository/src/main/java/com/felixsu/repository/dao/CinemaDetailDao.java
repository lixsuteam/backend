package com.felixsu.repository.dao;

import com.felixsu.repository.entity.cinema.CinemaDetailEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by felixsoewito on 11/21/16.
 * repository query method for cinema_detail table
 */

@Repository
public interface CinemaDetailDao extends BaseDao<CinemaDetailEntity, Long> {

    @Query("select e from CinemaDetailEntity e where e.active=true and e.cinemaId = :cinemaId")
    List<CinemaDetailEntity> findByCinemaId(@Param("cinemaId") String cinemaId);
}
