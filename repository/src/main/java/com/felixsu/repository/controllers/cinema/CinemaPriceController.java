package com.felixsu.repository.controllers.cinema;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.repository.controllers.RepositoryController;
import com.felixsu.repository.entity.cinema.CinemaPriceEntity;

import java.util.List;

/**
 * Created on 12/7/16.
 *
 * @author felixsoewito
 */
public interface CinemaPriceController
        extends RepositoryController<CinemaPrice, CinemaPriceEntity, Long> {

    RestResponse<CinemaPrice> batchUpdate(String cinemaId, List<CinemaPrice> prices);

}
