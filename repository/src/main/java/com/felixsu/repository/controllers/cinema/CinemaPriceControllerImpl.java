package com.felixsu.repository.controllers.cinema;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.CinemaPrice;
import com.felixsu.repository.controllers.BaseAuditableController;
import com.felixsu.repository.entity.cinema.CinemaPriceEntity;
import com.felixsu.repository.services.cinema.CinemaPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 12/7/16.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping("/cinema/prices")
public class CinemaPriceControllerImpl
        extends BaseAuditableController<CinemaPriceService, CinemaPriceEntity, CinemaPrice, Long>
        implements CinemaPriceController {

    @Autowired
    public CinemaPriceControllerImpl(CinemaPriceService service) {
        super(service);
    }

    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<CinemaPrice> batchUpdate(
            @PathVariable(name = "id") String cinemaId,
            @RequestBody List<CinemaPrice> prices) {
        try {
            service.pricesUpdate(cinemaId, prices);
            return wrapList(null, null);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }
}
