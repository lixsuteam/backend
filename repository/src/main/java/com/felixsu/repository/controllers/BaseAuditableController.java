package com.felixsu.repository.controllers;

import com.felixsu.springcommon.controller.BaseController;
import com.felixsu.repository.entity.AuditableEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.EntityModel;
import com.felixsu.common.model.RestResponse;
import com.felixsu.repository.services.base.BaseService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.Serializable;

/**
 * Created by felixsoewito on 11/13/16.
 */
public abstract class BaseAuditableController<S extends BaseService<E, M, ID>,
        E extends AuditableEntity,
        M extends EntityModel<ID>,
        ID extends Serializable>
        extends BaseController<M>
        implements RepositoryController<M, E, ID>{

    protected S service;

    public BaseAuditableController(S service) {
        this.service = service;
    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<M> create(@RequestBody() M model) {
        try {
            M result = service.create(model);
            return wrapObject(result, null);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<M> update(@RequestBody() M model) throws FSNotFoundException, FsBadRequestException {
        try {
            M result = service.update(model);
            return wrapObject(result, null);
        } catch (FSNotFoundException e) {
            throw new FSNotFoundException(e);
        } catch (FsBadRequestException e) {
            throw new FsBadRequestException(e);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.DELETE)
    @Override
    public RestResponse<M> delete(@PathVariable ID id) throws FSNotFoundException {
        try {
            service.delete(id);
            return wrapObject(null, null);
        } catch (FSNotFoundException e) {
            throw new FSNotFoundException(e);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.GET)
    @Override
    public RestResponse<M> findOne(@PathVariable ID id) throws FSNotFoundException {
        try {
            M result = service.findOne(id);
            return wrapObject(result, null);
        } catch (FSNotFoundException e) {
            throw new FSNotFoundException(e);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

}
