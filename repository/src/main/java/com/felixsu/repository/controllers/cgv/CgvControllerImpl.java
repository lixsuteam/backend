package com.felixsu.repository.controllers.cgv;

import com.felixsu.common.exception.FsMultipleChoicesException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.general.Acknowledge;
import com.felixsu.repository.services.cgv.CgvService;
import com.felixsu.springcommon.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by felixsoewito on 11/28/16.
 */

@RestController
@RequestMapping(path = "cgv")
public class CgvControllerImpl
        extends BaseController<Acknowledge>
        implements CgvController {

    private final CgvService service;

    @Autowired
    public CgvControllerImpl(CgvService service) {
        this.service = service;
    }

    @RequestMapping(
            path = "/cinemas",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Acknowledge> fetchCinemas() throws FsMultipleChoicesException {
        Acknowledge acknowledge = service.fetchCinemasAndSchedules();
        return wrapObject(acknowledge, null);
    }

    @RequestMapping(
            path = "/movies",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Acknowledge> fetchMovies() throws FsMultipleChoicesException {
        Acknowledge acknowledge = service.fetchMovies();
        return wrapObject(acknowledge, null);
    }
}
