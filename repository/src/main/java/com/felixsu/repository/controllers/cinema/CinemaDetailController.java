package com.felixsu.repository.controllers.cinema;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.repository.controllers.RepositoryController;
import com.felixsu.repository.entity.cinema.CinemaDetailEntity;

import java.util.List;

/**
 * Created on 12/7/16.
 *
 * @author felixsoewito
 */
public interface CinemaDetailController
        extends RepositoryController<CinemaDetail, CinemaDetailEntity, Long> {

    RestResponse<CinemaDetail> batchUpdate(String cinemaId, List<CinemaDetail> details);

}
