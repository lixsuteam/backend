package com.felixsu.repository.controllers.movie;

import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.repository.entity.cinema.MovieEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.common.model.RestResponse;
import com.felixsu.repository.controllers.BaseAuditableController;
import com.felixsu.repository.services.movie.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 11/15/16.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping("/movie")
public class MovieControllerImpl
        extends BaseAuditableController<MovieService, MovieEntity, Movie, String>
        implements MovieController {

    @Autowired
    public MovieControllerImpl(MovieService service) {
        super(service);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<Movie> findAll(
            @RequestParam(required = false, name = "status") String status,
            @RequestParam(required = false, name = "genre") String genre,
            @RequestParam(required = false, name = "city") String city,
            @RequestParam(required = false, name = "owner", defaultValue = Cinema.CGV) String owner) throws FSNotFoundException, FsBadRequestException {
        try {
            Movie.Status movieStatus = Movie.Status.STATUS_NOW_PLAYING;

            if (status != null) {
                switch (status) {
                    case "coming_soon":
                        movieStatus = Movie.Status.STATUS_COMING_SOON;
                        break;
                    case "all":
                        movieStatus = null;
                        break;
                    default:
                        throw new FsBadRequestException("bad request on status=" + status);
                }

            }
            List<Movie> result = service.findAll(movieStatus, genre, city, owner);
            return wrapList(result, null);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(
            path = "/movies",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<Movie> batchUpdate(
            @RequestBody List<Movie> movies,
            @RequestParam(required = false, name = "owner", defaultValue = Cinema.CGV) String owner) {
        try {
            service.batchUpdate(movies, owner);
            return wrapList(null, null);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

}
