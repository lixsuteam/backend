package com.felixsu.repository.controllers;

import com.felixsu.common.entity.RepositoryEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.EntityModel;
import com.felixsu.common.model.RestResponse;

import java.io.Serializable;

/**
 * Created by felixsoewito on 12/2/16.
 */
public interface RepositoryController<M extends EntityModel<ID>, E extends RepositoryEntity, ID extends Serializable> {

    RestResponse<M> create(M model);
    RestResponse<M> update(M model) throws FSNotFoundException, FsBadRequestException;
    RestResponse<M> delete(ID id) throws FSNotFoundException;
    RestResponse<M> findOne(ID id) throws FSNotFoundException;
}
