package com.felixsu.repository.controllers.cinema;

import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.CinemaDetail;
import com.felixsu.repository.controllers.BaseAuditableController;
import com.felixsu.repository.entity.cinema.CinemaDetailEntity;
import com.felixsu.repository.services.cinema.CinemaDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 12/7/16.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping("/cinema/details")
public class CinemaDetailControllerImpl
        extends BaseAuditableController<CinemaDetailService, CinemaDetailEntity, CinemaDetail, Long>
        implements CinemaDetailController {

    @Autowired
    public CinemaDetailControllerImpl(CinemaDetailService service) {
        super(service);
    }

    @RequestMapping(
            path = "/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<CinemaDetail> batchUpdate(
            @PathVariable(name = "id") String cinemaId,
            @RequestBody List<CinemaDetail> details) {

        try {
            service.detailsUpdate(cinemaId, details);
            return wrapList(null, null);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }
}
