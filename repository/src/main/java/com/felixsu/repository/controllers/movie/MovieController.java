package com.felixsu.repository.controllers.movie;

import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.repository.entity.cinema.MovieEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Movie;
import com.felixsu.repository.controllers.RepositoryController;

import java.util.List;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public interface MovieController extends RepositoryController<Movie, MovieEntity, String> {

    RestResponse<Movie> findAll(String status, String genre, String city, String owner)
            throws FSNotFoundException, FsBadRequestException;

    RestResponse<Movie> batchUpdate(List<Movie> movies, String owner);

}
