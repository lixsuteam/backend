package com.felixsu.repository.controllers.health;

import com.felixsu.springcommon.controller.BaseController;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.general.Health;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 11/10/16.
 *
 * @author felixsoewito
 */
@RestController()
@RequestMapping(path = "/")
public class HealthControllerImpl
        extends BaseController<Health>
        implements HealthController {

    @RequestMapping(method = RequestMethod.GET)
    @Override
    public RestResponse<Health> status() {
        return wrapObject(new Health("OK", "200"), null);
    }
}
