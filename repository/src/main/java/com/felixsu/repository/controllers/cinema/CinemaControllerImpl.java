package com.felixsu.repository.controllers.cinema;

import com.felixsu.repository.entity.cinema.CinemaEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.RestResponse;
import com.felixsu.repository.controllers.BaseAuditableController;
import com.felixsu.repository.services.cinema.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created on 11/15/16.
 *
 * @author felixsoewito
 */
@RestController
@RequestMapping("/cinema")
public class CinemaControllerImpl
        extends BaseAuditableController<CinemaService, CinemaEntity, Cinema, String>
        implements CinemaController {

    @Autowired
    public CinemaControllerImpl(CinemaService service) {
        super(service);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<Cinema> findAll(
            @RequestParam(required = false, name = "city") String city,
            @RequestParam(required = false, name = "owner", defaultValue = Cinema.CGV) String owner) throws FSNotFoundException {
        try {
            List<Cinema> result = service.findAll(city, owner);
            return wrapList(result, null);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(
            path = "/cinemas",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public RestResponse<Cinema> batchUpdate(
            @RequestBody List<Cinema> cinemas,
            @RequestParam(required = false, name = "owner", defaultValue = Cinema.CGV) String owner) {
        try {
            service.batchUpdate(cinemas, owner);
            return wrapList(null, null);
        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

}
