package com.felixsu.repository.controllers.schedule;

import com.felixsu.repository.entity.cinema.ScheduleEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.repository.controllers.BaseAuditableController;
import com.felixsu.repository.services.schedule.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by felixsoewito on 11/15/16.
 * Schedule Controller
 */

@RestController
@RequestMapping("/schedule")
public class ScheduleControllerImpl
        extends BaseAuditableController<ScheduleService, ScheduleEntity, Schedule, Long>
        implements ScheduleController {

    @Autowired
    public ScheduleControllerImpl(ScheduleService service) {
        super(service);
    }

    @RequestMapping(
            path = "cinema/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Schedule> findScheduleByCinema(
            @PathVariable String id) throws FSNotFoundException {
        List<Schedule> result = service.findScheduleCinema(id);
        return wrapList(result, null);
    }

    @RequestMapping(
            path = "movie/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Schedule> findScheduleByMovie(
            @PathVariable String id) throws FSNotFoundException {
        List<Schedule> result = service.findScheduleMovie(id);
        return wrapList(result, null);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Schedule> findAll(
            @RequestParam(required = false, name = "city") String city,
            @RequestParam(required = false, name = "movie") String movieId,
            @RequestParam(required = false, name = "cinema") String cinemaId) throws FSNotFoundException {
        List<Schedule> result = service.findAll(city, movieId, cinemaId);
        return wrapList(result, null);
    }

    @RequestMapping(
            path = "/schedules/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Override
    public RestResponse<Schedule> updateSchedules(@PathVariable(name = "id") String cinemaId, @RequestBody List<Schedule> schedules) {
        service.updateSchedules(cinemaId, schedules);
        return wrapList(null, null);
    }
}
