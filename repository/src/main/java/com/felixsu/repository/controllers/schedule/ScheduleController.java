package com.felixsu.repository.controllers.schedule;

import com.felixsu.repository.entity.cinema.ScheduleEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Schedule;
import com.felixsu.repository.controllers.RepositoryController;

import java.util.List;

/**
 * Created by felixsoewito on 12/2/16.
 */
public interface ScheduleController extends RepositoryController<Schedule, ScheduleEntity, Long> {

    RestResponse<Schedule> findScheduleByCinema(String id) throws FSNotFoundException;

    RestResponse<Schedule> findScheduleByMovie(String id) throws FSNotFoundException;

    RestResponse<Schedule> findAll(String city, String movieId, String cinemaId) throws FSNotFoundException;

    RestResponse<Schedule> updateSchedules(String cinemaId, List<Schedule> schedules);

}
