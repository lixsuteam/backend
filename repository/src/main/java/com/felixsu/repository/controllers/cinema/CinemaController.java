package com.felixsu.repository.controllers.cinema;

import com.felixsu.repository.entity.cinema.CinemaEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.model.RestResponse;
import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.repository.controllers.RepositoryController;

import java.util.List;

/**
 * Created on 12/2/16.
 *
 * @author felixsoewito
 */
public interface CinemaController
        extends RepositoryController<Cinema, CinemaEntity, String> {

    RestResponse<Cinema> findAll(String city, String owner) throws FSNotFoundException;

    RestResponse<Cinema> batchUpdate(List<Cinema> cinemas, String owner);

}
