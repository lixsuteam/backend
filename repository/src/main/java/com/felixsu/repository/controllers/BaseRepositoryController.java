package com.felixsu.repository.controllers;

import com.felixsu.common.entity.RepositoryEntity;
import com.felixsu.common.exception.FSNotFoundException;
import com.felixsu.common.exception.FsBadRequestException;
import com.felixsu.common.model.EntityModel;
import com.felixsu.common.model.RestResponse;
import com.felixsu.repository.services.base.BaseService;

import java.io.Serializable;

/**
 * Created by felixsoewito on 11/13/16.
 * Base non Auditable Controller
 */
public class BaseRepositoryController<S extends BaseService<E, M, ID>,
        E extends RepositoryEntity,
        M extends EntityModel<ID>,
        ID extends Serializable> implements RepositoryController<M, E, ID> {

    private S service;

    public BaseRepositoryController(S service) {
        this.service = service;
    }

    @Override
    public RestResponse<M> create(M model) {
        return null;
    }

    @Override
    public RestResponse<M> update(M model) throws FSNotFoundException, FsBadRequestException {
        return null;
    }

    @Override
    public RestResponse<M> delete(ID id) throws FSNotFoundException {
        return null;
    }

    @Override
    public RestResponse<M> findOne(ID id) throws FSNotFoundException {
        return null;
    }
}
