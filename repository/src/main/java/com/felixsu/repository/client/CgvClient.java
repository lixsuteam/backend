package com.felixsu.repository.client;

import com.felixsu.common.helper.DateHelper;
import com.felixsu.common.model.extra.CgvItem;
import com.felixsu.repository.worker.cgv.CgvCinemaWorker;
import com.felixsu.repository.worker.cgv.CgvMovieWorker;
import com.felixsu.springcommon.client.entity.CinemaEntityClient;
import com.felixsu.springcommon.client.entity.MovieEntityClient;
import com.felixsu.springcommon.client.entity.ScheduleEntityClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by felixsoewito on 11/24/16.
 */

public class CgvClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(CgvClient.class);

    private final CinemaEntityClient cinemaEntityClient;
    private final ScheduleEntityClient scheduleEntityClient;
    private final MovieEntityClient movieEntityClient;

    public CgvClient(CinemaEntityClient cinemaEntityClient,
                     ScheduleEntityClient scheduleEntityClient,
                     MovieEntityClient movieEntityClient) {
        this.cinemaEntityClient = cinemaEntityClient;
        this.scheduleEntityClient = scheduleEntityClient;
        this.movieEntityClient = movieEntityClient;
    }

    public void fetchCinemaAndSchedule(OnFinishListener listener) {
        LOGGER.debug("start load cgv cinema at " + DateHelper.getIso8601DateFormatterString());
        new CgvCinemaWorker(listener, cinemaEntityClient, scheduleEntityClient).start();
    }

    public void fetchMovie(OnFinishListener listener){
        LOGGER.debug("start load cgv movie at " + DateHelper.getIso8601DateFormatterString());
        new CgvMovieWorker(listener, movieEntityClient).start();
    }

    public interface OnFinishListener {

        <T extends CgvItem> void onFinish(Throwable e, List<T> results);

    }
}
