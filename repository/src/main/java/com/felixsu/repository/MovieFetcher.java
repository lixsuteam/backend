package com.felixsu.repository;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Movie;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 20/06/18.
 *
 * @author Felix Soewito
 */
public class MovieFetcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(MovieFetcher.class);
    static final String baseUrl = "https://www.cgv.id";

    public static void main(String[] args) {

        final List<String> nowPlayingUrls = new ArrayList<>();
        final List<String> comingSoonUrls = new ArrayList<>();

        final Consumer<Element> nowPlayingUrlConsumer = e -> {
            nowPlayingUrls.add(baseUrl + e.attr("href"));
        };
        final Consumer<Element> comingSoonUrlConsumer = e -> {
            comingSoonUrls.add(baseUrl + e.attr("href"));
        };

        try {
            Document doc = Jsoup.connect("https://www.cgv.id/en/movies/now_playing")
                    .timeout(10000)
                    .get();

            doc
                    .body()
                    .getElementsByClass("movie-list-body").get(0)
                    .getElementsByTag("a")
                    .forEach(nowPlayingUrlConsumer);
            doc
                    .body()
                    .getElementsByClass("comingsoon-movie-list-body").get(0)
                    .getElementsByTag("a")
                    .forEach(comingSoonUrlConsumer);

//            String sample = nowPlayingUrls.get(3);
//            System.out.println(sample);
            Movie m = readMovie("https://www.cgv.id/en/movies/info/18015800", true);
            System.out.println("test");
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    static Movie readMovie(String url, boolean isNowPlaying) throws IOException {
        Document movieDoc = Jsoup.connect(url)
                .timeout(10000)
                .get();

        Movie movie = new Movie();
        try {
            String[] urlComponents = url.split("/");
            movie.setId(urlComponents[urlComponents.length-1]);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new RuntimeException("Failed to read movie ID");
        }

        if (isNowPlaying) {
            movie.setStatus(Movie.Status.STATUS_NOW_PLAYING);
        } else {
            movie.setStatus(Movie.Status.STATUS_COMING_SOON);
        }

        movie.setOwner(Cinema.CGV);

        setCoverUrl(movieDoc, movie);
        setTrailerUrl(movieDoc, movie);
        setMovieTitle(movieDoc, movie);
        setMovieSynopsis(movieDoc, movie);
        setMovieProperties(movieDoc, movie);

        return movie;
    }

    static void setCoverUrl(Document movieDoc, Movie movie) {
        try {
            String coverUrl = movieDoc
                    .getElementsByClass("poster-section").get(0)
                    .getElementsByTag("img").get(0)
                    .attr("src");
            movie.setCoverUrl(baseUrl + coverUrl);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            movie.setCoverUrl("ERROR");
        }
    }

    static void setTrailerUrl(Document movieDoc, Movie movie) {
        try {
            String trailerUrl = movieDoc
                    .getElementsByClass("trailer-section").get(0)
                    .getElementsByTag("iframe").get(0)
                    .attr("src");
            movie.setTrailerUrl(trailerUrl);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            movie.setTrailerUrl("ERROR");
        }
    }

    static void setMovieSynopsis(Document movieDoc, Movie movie) {
        try {
            List<String> synopsis =  movieDoc
                    .getElementsByClass("movie-synopsis").get(0)
                    .textNodes()
                    .stream()
                    .map(TextNode::text)
                    .filter(s -> !s.trim().isEmpty())
                    .collect(Collectors.toList());
            movie.setMovieDescription(synopsis.get(synopsis.size()-1));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            movie.setMovieDescription("ERROR");
        }
    }

    static void setMovieTitle(Document movieDoc, Movie movie) {
        try {
            final String title = movieDoc
                    .getElementsByClass("movie-info-title").get(0)
                    .text();
            movie.setMovieName(title);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            movie.setMovieName("ERROR");
        }
    }

    static void setMovieProperties(Document movieDoc, Movie movie) {
        try {
            List<Element> elements = movieDoc
                    .getElementsByClass("movie-add-info").get(0)
                    .getElementsByTag("li");

            for (int i = 0; i < elements.size(); i++) {
                try {
                    String text = elements.get(i).text();
                    if (text.contains("STARRING")) {
                        movie.setActors(text.split(":")[1].trim());
                    }
                    if (text.contains("DIRECTOR")) {
                        movie.setDirectors(text.split(":")[1].trim());
                    }
                    if (text.contains("CENSOR")) {
                        movie.setRating(text.split(":")[1].trim());
                    }
                    if (text.contains("GENRE")) {
                        movie.setGenre(text.split(":")[1].trim());
                    }
                    if (text.contains("LANGUAGE")) {
                        movie.setLanguage(text.split(":")[1].trim());
                    }
                    if (text.contains("SUBTITLE")) {
                        movie.setSubtitle(text.split(":")[1].trim());
                    }
                    if (text.contains("DURATION")) {
                        int duration = 0;
                        try {
                            duration = Integer.valueOf(text
                                    .split(":")[1].trim()
                                    .split(" ")[0].trim());
                        } catch (Exception e) {
                            LOGGER.error(e.getMessage(), e);
                        }
                        movie.setDuration(duration);
                    }
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
