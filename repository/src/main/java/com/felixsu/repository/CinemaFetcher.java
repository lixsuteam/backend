package com.felixsu.repository;

import com.felixsu.common.model.cinema.Cinema;
import com.felixsu.common.model.cinema.Schedule;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created on 23/06/18.
 *
 * @author Felix Soewito
 */
public class CinemaFetcher {
    private static final boolean IS_DEMO = true;
    private static final Logger LOGGER = LoggerFactory.getLogger(CinemaFetcher.class);
    static final String baseUrl = "https://www.cgv.id";
    static final List<Cinema> mainContainer = Collections.synchronizedList(new ArrayList<>());

    public static void main(String[] args) {
        try {
            Document doc = Jsoup.connect("https://www.cgv.id/en/schedule/cinema")
                    .timeout(10000)
                    .get();
            System.out.println("load main cinema page done");

            List<Element> citiesElements = new ArrayList<>();
            citiesElements.addAll(doc.body().getElementsByClass("city on"));
            citiesElements.addAll(doc.body().getElementsByClass("city"));
            System.out.println("cities elements: " + citiesElements.size());

            citiesElements.forEach(new CinemaConsumer());

            mainContainer.sort((o1, o2) -> {
                int id1 = Integer.valueOf(o1.getId());
                int id2 = Integer.valueOf(o2.getId());
                return id1 - id2;
            });

            System.out.println("iterate result: " + mainContainer.size());
            for (Cinema cinema : mainContainer) {
                System.out.println(cinema.getId() + "-" + cinema.getCinemaName());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static class CinemaConsumer implements Consumer<Element> {

        @Override
        public void accept(Element e) {
            final String city = e.getElementsByTag("a").get(0).text();
            List<Cinema> datas = e.getElementsByTag("a")
                    .stream()
                    .filter(e1 -> e1.hasClass("cinema_fav"))
                    .map(e1 -> {
                        String id = e1.attr("id");
                        String cinemaName = e1.attr("title");
                        String url = baseUrl + e1.attr("href");

                        Cinema cinema = new Cinema();
                        cinema.setId(id);
                        cinema.setOwner(Cinema.CGV);
                        cinema.setCinemaName(cinemaName);
                        cinema.setCity(city);

                        if (IS_DEMO && id.equals("002")) {
                            readCompleteCinemaData(url, cinema);
                        } else {
                            readCompleteCinemaData(url, cinema);
                        }

                        return cinema;
                    })
                    .collect(Collectors.toList());
            mainContainer.addAll(datas);
        }

        private void readCompleteCinemaData(String cinemaUrl, final Cinema cinema) {
            try {
                Document doc = Jsoup.connect(cinemaUrl)
                        .timeout(10000)
                        .get();

                final String cinemaCover = doc.body()
                        .getElementsByClass("cinema-info").get(0)
                        .getElementsByTag("img").get(0)
                        .attr("src");
                cinema.setCover(cinemaCover);

                List<Element> elements = doc.body()
                        .getElementsByClass("schedule-lists").get(0)
                        .getElementsByClass("schedule-title");

                if (IS_DEMO) {
                    final Element element = elements.get(0);
                    final String movieName = element
                            .getElementsByTag("a").get(0)
                            .text().trim();
                    final String movieId = element
                            .getElementsByTag("a").get(0)
                            .attr("href")
                            .split("/")[4];
                    final String[] scheduleDetails = element
                            .getElementsByTag("span").get(0)
                            .text().split("/");

                    element.nextElementSibling()
                            .getElementsByClass("schedule-type")
                            .forEach(e -> {
                                final Schedule schedule = new Schedule();
                                schedule.setMovieId(movieId);
                                schedule.setCinemaId(cinema.getId());
                                schedule.setCinemaName(cinema.getCinemaName());
                                schedule.setCity(cinema.getCity());

                                schedule.setType("NA");
                                schedule.setPrice("NA");
                                schedule.setFormat(e.ownText().trim());

                                String schedules = e.nextElementSibling().getElementsByTag("a")
                                        .stream()
                                        .map(timeElement -> timeElement.ownText().trim())
                                        .collect(Collectors.joining("#"));
                                schedule.setPlayingTime(schedules);
                            });
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }
}
